import { red } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
	palette: {
		primary: {
			light: '#499AB5',
			main: '#06759A',
			dark: '#03485F',

		},
		secondary: {
			light: '#FFB260',
			main: '#F77F00',
			dark: '#984E00',
			contrastText: "#fff"
		},
		secondaryAlt: {
			lighter: '#576AC0',
			light: '#374DB2',
			main: '#152FA9',
			dark: '#0F2484',
			darker: '#091A68',
		},
		danger: {
			main: red.A400,
			dark: red.A700,
			contrastText: '#fff',
		},
		error: {
			main: red.A400,
		},
		background: {
			default: '#fff',
			black: '#2e282a',
		},
	},
	status: {
		danger: '#ff0033',
		success: '#4bb543',
	},
});

export default theme;
