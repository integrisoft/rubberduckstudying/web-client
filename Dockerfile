FROM node:10.15.0 as build

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json ./
RUN npm install --silent --production

COPY . .
RUN npm run build

# Using Ubuntu

EXPOSE 3000

CMD ["npm", "start"]