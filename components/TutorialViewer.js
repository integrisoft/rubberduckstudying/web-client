import { Breadcrumbs, Card, CardContent, Container, CssBaseline, Divider, Typography } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/styles';
import pluralize from 'pluralize';
import React from 'react';
import { connect } from 'react-redux';
import readingTime from 'reading-time';
import { formatUtcDate } from '../shared/dateformat';
import Link from './Link';
import ResourceDrawer from './ResourceDrawer';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexDirection: 'row',
		padding: '20px 0px 0px 0px',
		height: '100%',
	},
	// toolbar: theme.mixins.toolbar,
	contentContainer: {
		padding: '48px 24px 0 24px',
		alignContent: 'center',
		alignItems: 'center',
	},
	content: {
		display: 'flex',
		flexGrow: 1,
		padding: '48px 24px',
		// padding: theme.spacing(3),
	},
	breadCrumbNav: {
		marginBottom: '25px',
	},
	header: {
		// marginBottom: '15px',
		padding: '0px 24px',

	},
	title: {
		marginBottom: '10px',
	},
	description: {
		width: '100%',
		fontSize: '15px',
		wordBreak: 'break-word',
		overflowWrap: 'break-word',
	},
	editButton: {
		marginLeft: '10px',
		"&:hover": {
			cursor: 'pointer',
		},
		verticalAlign: 'text-bottom',
	},
	footerInfo: {
		padding: '32px 0px',
	},
	footerInfoHeader: {
		// color: theme.palette.secondary.main,
		// letterSpacing: '0.05em',
		fontSize: '15px',
		fontWeight: 300,
		opacity: '0.9',
		textTransform: 'uppercase',
	},
	scrollableRow: {
		overflowX: 'auto',
		flexFlow: 'row noWrap',
		display: 'flex',
		flexDirection: 'row',
		padding: '5px',
	},
	scrollableRowItem: {
		overflowX: 'hidden',
		minWidth: '150px',
		width: '250px',
		margin: '10px 10px 10px 0px',
		'&:hover': {
			cursor: 'pointer',
			boxShadow: '0 5px 15px rgba(0,0,0,0.3)',
		}
	},
}));

function TutorialViewer({ tutorial, currentUser }) {
	const classes = useStyles()

	if (currentUser == null) {
		return ""
	}
	const readingTimeStats = readingTime(tutorial.content)

	return (
		<div className={classes.root} >
			<CssBaseline />
			<Container maxWidth="md" className={classes.contentContainer}>
				<header className={classes.header}>
					<Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumbNav}>
						<Link color="inherit" href={`/course/${tutorial.parentGoal.parentUnit.parentCourse.slug}`}>
							{tutorial.parentGoal.parentUnit.parentCourse.name}
						</Link>
						<Typography color='textPrimary'>{tutorial.parentGoal.parentUnit.name}</Typography>
						<Link color="inherit" href={`/course/${tutorial.parentGoal.parentUnit.parentCourse.slug}/${tutorial.parentGoal.parentUnit.slug}/${tutorial.parentGoal.slug}`}>
							{tutorial.parentGoal.name}
						</Link>
						<span>
							<Typography component='span' color='textPrimary'>{tutorial.title}</Typography>
							{tutorial.parentGoal.parentUnit.parentCourse.courseRole.manageTutorial || tutorial.authorID == currentUser.id ?
								<Link href={`/course/${tutorial.parentGoal.parentUnit.parentCourse.slug}/${tutorial.parentGoal.parentUnit.slug}/${tutorial.parentGoal.slug}/tutorial/edit/${tutorial.id}`} >
									<EditIcon className={classes.editButton} fontSize='small' />
								</Link> :
								""
							}
						</span>
					</Breadcrumbs>
					<Typography variant='h1' className={classes.title} >{tutorial.title}</Typography>
					<span>
						<Typography component='span' className={classes.headerText} variant='subtitle1'>{formatUtcDate(tutorial.updatedAt)}</Typography>
						<Typography component='span' className={classes.headerText} variant='subtitle1'> · </Typography>
						<Typography component='span' variant='subtitle1'>{readingTimeStats.text}</Typography>
					</span>
				</header>
				<main className={classes.content}>
					<div className={classes.descriptionContainer}>
						<div className={classes.description} dangerouslySetInnerHTML={{ __html: tutorial.content }} />
					</div>
				</main>
				<Divider />
				<footer>
					<div className={classes.footerInfo}>
						<Typography variant='body1' gutterBottom className={classes.footerInfoHeader}>Written By</Typography>
						<Link href={`/user/${tutorial.author.username}`}><Typography gutterBottom variant='h2'>{tutorial.author.username}</Typography></Link>
						<Typography variant='subtitle2'>{tutorial.author.bio}</Typography>
					</div>
					<Divider />
					<div className={classes.footerInfo}>
						<Typography variant='body1' gutterBottom className={classes.footerInfoHeader}>Other Tutorials For This Goal</Typography>
						<div className={classes.scrollableRow}>
							{tutorial.parentGoal.tutorials.map(t => {
								if (t.id == tutorial.id) {
									return ""
								}
								return (
									<Link href={`/course/${tutorial.parentGoal.parentUnit.parentCourse.slug}/${tutorial.parentGoal.parentUnit.slug}/${tutorial.parentGoal.slug}/tutorial/${t.id}`} >
										<Card key={t.id} className={classes.scrollableRowItem}>
											<CardContent>
												<Typography className={classes.title} color="textSecondary" gutterBottom>
													{t.title}
												</Typography>
												<Typography>
													{t.stars.length} {pluralize('star', t.stars.length)}
												</Typography>
												<Typography>
													{readingTime(t.content).text}
												</Typography>
											</CardContent>
										</Card>
									</Link>
								)
							})}
						</div>
					</div>
				</footer>
			</Container>
			<ResourceDrawer />
		</div >
	)
}

const mapStateToProps = state => ({
	currentUser: state.user.currentUser,
})

const mapDispatchToProps = () => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(TutorialViewer)