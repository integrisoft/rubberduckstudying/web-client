import { Divider, ExpansionPanelDetails, List, ListItem, makeStyles, Typography } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import pluralize from 'pluralize';
import React from 'react';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		width: '100%',
		padding: '20px 15px 20px 15px',
	},
	title: {
		marginBottom: '30px',
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary,
	},
	icon: {
		verticalAlign: 'bottom',
		height: 20,
		width: 20,
	},
	details: {
		alignItems: 'center',
	},
	columnSmall: {
		flexBasis: '33.333%',
	},
	columnLarge: {
		flexBasis: '66.666%',
	},
	helper: {
		borderLeft: `2px solid ${theme.palette.divider}`,
		padding: theme.spacing(1, 2),
	},
	link: {
		color: theme.palette.primary.main,
		textDecoration: 'none',
		'&:hover': {
			textDecoration: 'underline',
		},
	},
	courseListItem: {
		padding: '15px 24px',
	},
	newAction: {
		display: 'block',
		marginTop: '20px',
	},
	grow: {
		flexGrow: 1,
	},
	nameTextField: {
		width: '100%',
	},
	description: {
		padding: '16px 24px',
		wordBreak: 'break-all',
		// width: '100%',
		display: 'block',
	},
	expansionPanel: {
		width: '100%',
		padding: '0',
	},
	expansionPanelSummary: {
		backgroundColor: '#f9f9f9',
		border: 'solid 1px #e8e9eb',
		// padding:
		minHeight: '48px',
		// height: 'auto',
		// width: '100%',
	},
	expansionDetails: {
		padding: '0',
	},
	divider: {
		width: '100%',
		margin: '20px 0',
	},
	unitList: {
		width: '100%',
		padding: '0',
	},
}));

export default function UnitGoalViewer({ course }) {
	const classes = useStyles()
	const units = course.units
	return (
		<>
			{units.map((unit, unitIndex) => {
				return (
					<ExpansionPanel className={classes.expansionPanel} defaultExpanded key={`unit-${unitIndex}`}>
						<ExpansionPanelSummary
							className={classes.expansionPanelSummary}
							// expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1c-content"
							id="panel1c-header"
						>
							<div className={classes.columnLarge}>
								<Typography className={classes.heading}>{unit.name}</Typography>
							</div>
							<div className={classes.columnSmall}>
								{unit.goals.length} {pluralize("goal", unit.goals.length)}
							</div>
						</ExpansionPanelSummary>
						<div className={classes.description}>
							<Typography variant='h6'>Description</Typography>
							<div dangerouslySetInnerHTML={{ __html: unit.description }} />
							{/* <Typography variant='h6'>Goals</Typography> */}
						</div>
						<Divider />
						<ExpansionPanelDetails className={classes.expansionDetails}>
							<List className={classes.unitList}>
								{unit.goals.map((goal, goalIndex) => {
									return (
										<React.Fragment key={`unit-${unitIndex}-goal-${goalIndex}`}>
											<ListItem className={classes.courseListItem} key={`goal-${unitIndex}-${goalIndex}`} alignItems="flex-start">
												<div className={classes.columnLarge}>
													<Link href={`/course/${course.slug}/${unit.slug}/${goal.slug}`}><Typography className={classes.heading}>{goal.name}</Typography></Link>
												</div>
												<div className={classes.columnSmall}>
													{goal.tutorials.length} {pluralize("tutorial", goal.tutorials.length)}
												</div>
											</ListItem>
											<Divider />
										</React.Fragment>
									)
								})}
							</List>
						</ExpansionPanelDetails>
					</ExpansionPanel>
				)
			})}
		</>
	)
}