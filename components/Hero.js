import { Container } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import Link from './Link';
import NavBar from './NavBar';

const useStyles = makeStyles(theme => ({
	button: {
		fontWeight: '500',
		fontSize: '14px',
		textTransform: 'none',
		// textStroke: '1px black'
		// color: 'black'
	},
	heroContainer: {
		display: 'flex',
		flexDirection: 'column',
		flexGrow: '1',
		flexWrap: 'wrap',
		alignItems: 'center',
		alignContent: 'center',
		justifyContent: 'center',
		[theme.breakpoints.up('lg')]: {
			padding: '0px 40px 0 40px',
		},
		height: '85%',


	},
	heroGridContainer: {
		flex: '1',
		alignItems: 'center',
		alignContent: 'center',
		height: '100%',
	},
	heroTextContainer: {
		flex: '1',
		display: 'flex',
		flexDirection: 'column',
		// [theme.breakpoints.down('sm')]: {
		alignItems: 'center',
		alignContent: 'center',
		justifyContent: 'center',
		// backgroundColor: 'red',
		// },
		// backgroundColor: "#000",
		color: '#fff',
		textAlign: 'center',
		fontWeight: 400,
	},
	image: {
		width: 600,
		// height: 600,
		[theme.breakpoints.down('md')]: {
			width: 500,
			// height: 500,
		}
	},
	img: {
		margin: 'auto',
		display: 'block',
		maxWidth: '100%',
		maxHeight: '100%',
	},
	hero: {
		background: 'rgb(73,154,181)',
		background: 'linear-gradient(0deg, rgba(73,154,181,1) 0%, rgba(3,72,95,1) 100%)',
		// backgroundImage: 'url(/static/images/hero-bg.svg)',
		backgroundImage: 'url(/static/images/helloquence-5fNmWej4tAA-unsplash.jpg)',
		// height: '752px',
		height: '500px',
		[theme.breakpoints.down('lg')]: {
			// height: '300px',
		},
		[theme.breakpoints.down('md')]: {
			// height: '275px',
		},
		[theme.breakpoints.down('sm')]: {
			display: 'flex',
			flexDirection: 'column',
			// flexGrow: '1',
			flexWrap: 'nowrap',
			alignItems: 'center',
			alignContent: 'center',
			maxHeight: '520px',
		},
		backgroundSize: 'cover',
	},
	overlay: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%',
		position: 'relative',
		zIndex: 1,
	},
	heroText: {
		alignItems: 'center',
		alignContent: 'center',
		[theme.breakpoints.down('sm')]: {
			textAlign: 'center',
		}
	},
	heroTitle: {
		// [theme.breakpoints.up('xs')]: {
		fontSize: '44px',
		// },
		[theme.breakpoints.down('md')]: {
			fontSize: '34px',
		},
		fontWeight: '500',
		lineHeight: '1.1em',
		fontStyle: 'normal',
		marginBottom: '20px',
		color: '#f7f7f7',
		// textShadow: '1px 1px #666666;',
	},
	heroSubtitle: {
		margin: '0',
		lineHeight: '1.7em',
		opacity: '.9',
		color: '#fff',
		marginBottom: '30px',
		fontSize: '16px',
		[theme.breakpoints.down('md')]: {
			fontSize: '16px',
		},
	},
}));

// class NavBar extends Component {
export default (props) => {
	const classes = useStyles();
	return (
		<>
			<NavBar transparent />
			<div className={classes.hero}>
				<div className={classes.overlay}>

					<Container maxWidth='sm' className={classes.heroContainer}>
						{/* <Grid container spacing={0} justify='center' className={classes.heroGridContainer}> */}
						{/* <Grid item xs={12} sm justify='center' container className={classes.heroTextContainer}> */}
						{/* <Grid item xs container className={classes.heroTextContainer} direction="column" spacing={0} > */}
						<div className={classes.heroTextContainer}>
							<div className={classes.heroText}>
								<h1 className={classes.heroTitle}>The World is Your Classroom</h1>
								<p className={classes.heroSubtitle}>
									With RubberDuckStudying, you are the teacher.
									Choose a subject, connect with students from around the world,
									and teach each other the material using free online resources.
									</p>
								<div>
									<Link href="/signup" className={classes.button}>
										<Button variant="contained" color="primary" className={classes.button}>Get Started</Button>
									</Link>
								</div>
							</div>
						</div>
						{/* </Grid>
						</Grid> */}
						{/* <Hidden smDown>
							<Grid item>
								<div className={classes.image}>
									<img className={classes.img} alt="complex" src="/static/images/undraw_youtube_tutorial_2gn3.svg" />
								</div>
							</Grid>
						</Hidden> */}
						{/* </Grid> */}
					</Container >
				</div>
			</div >
		</>
	)
}
