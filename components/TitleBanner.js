import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
	root: {
		// backgroundColor: theme.palette.secondaryAlt.light,
		// background: 'linear-gradient(220deg,#EC5252 -30%,#6E1A52 70%)',
		background: `linear-gradient(220deg,${theme.palette.primary.light} -30%,${theme.palette.primary.dark} 70%)`,
		// backgroundColor: theme.palette.background.black,
		// background: 'url(https://raw.github.com/mmoustafa/Chalkboard/master/img/bg.png)',
		color: '#fff',
		padding: '2em 0px',
		marginBottom: '20px',
	},
	bannerTitle: {
		[theme.breakpoints.down('md')]: {
			fontSize: '36px',
		},
		[theme.breakpoints.down('sm')]: {
			fontSize: '24px',
		},
	},
}))
export default ({ title }) => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<Container maxWidth='lg'>
				<Typography variant='h1' className={classes.bannerTitle}>{title}</Typography>
			</Container>
		</div>
	)
}