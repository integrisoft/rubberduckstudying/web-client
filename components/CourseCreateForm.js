import { Button, Container, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import dynamic from "next/dynamic";
import { connect } from 'react-redux';
import AutoComplete from './AutoComplete';
import CourseImageViewer from './CourseImageViewer';
import UnitEditForm from './UnitEditForm';
const DynamicTextEditor = dynamic(() => import("./TextEditor"), { ssr: false }); <DynamicTextEditor />

const useStyles = makeStyles(theme => ({
	root: {
		// textAlign: 'center',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		width: '100%',
		paddingBottom: '50px',
	},
	header: {
		padding: '50px 0 50px 0',
		// padding: '30px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignContent: 'center',
		alignItems: 'center',
		width: '100%',
		backgroundColor: theme.palette.background.black,
		background: 'url(https://raw.github.com/mmoustafa/Chalkboard/master/img/bg.png)',
		textAlign: 'center',
		marginBottom: '40px',
	},
	headerContainer: {
		width: '100%',
		display: 'flex',
		flexWrap: 'wrap',
		[theme.breakpoints.down('md')]: {
			flexDirection: 'column',
		},
		alignContent: 'center',
		alignItems: 'center',
		justifyContent: 'space-around',
	},
	headerTextContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
	},
	headerText: {
		color: '#fff',
		fontSize: '18px',
		marginBottom: '10px',
	},
	headerTitle: {
		color: '#fff',
		marginTop: '25px',
		marginBottom: '40px',
		fontSize: '46px',
		[theme.breakpoints.down('sm')]: {
			fontSize: '32px',
		},
		// alignSelf: 'center',
		textAlign: 'center',
	},
	photoContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
	},
	form: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		width: '100%',
		minWidth: '300px',
		alignContent: 'center',
		alignItems: 'center',
	},
	textField: {
		width: '100%',
		marginTop: '10px',
	},
	button: {
		marginTop: '30px',
		marginBottom: '30px',
		width: '300px',
	},
}));

function Form({ name, setName, description, setDescription, units, tags, setUnits, setTags, onSubmit, buttonText, allTags }) {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<div className={classes.header}>
				<Container maxWidth='lg' className={classes.headerContainer}>
					<Container maxWidth='sm' className={classes.headerContainer}>
						<div className={classes.photoContainer}>
							<CourseImageViewer captionColor='light' editMode={true} />
						</div>
					</Container>
					<Container maxWidth='sm' className={classes.headerTextContainer}>
						<Typography className={classes.headerTitle} variant='h1'>{name}</Typography>
					</Container>
				</Container>
			</div>
			<Container maxWidth="lg">
				<form className={classes.form} onSubmit={onSubmit}>
					{/* <Button variant="contained" color='primary' type='submit' className={classes.button}>{buttonText}</Button> */}
					<TextField
						required
						id="name"
						label="Course Name"
						className={classes.textField}
						value={name}
						onChange={e => setName(e.target.value)}
						margin="normal"
						inputProps={{
							maxLength: 50,
						}}
					/>
					<div className={classes.textField}>
						<AutoComplete initialValues={tags} onSelect={setTags} suggestions={allTags} />
					</div>
					<DynamicTextEditor maxLength={5000} id="courseDescription" height="350px" theme="snow" value={description} placeholder="Description" onChange={(value) => { setDescription(value) }} />
					<UnitEditForm units={units} setUnits={setUnits} />
					<Button variant="contained" color='primary' type='submit' className={classes.button}>{buttonText}</Button>
				</form>
			</Container>
		</div>
	)
}

class CourseCreateForm extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			name: "",
			description: "",
			units: [],
			tags: [],
		}
		if (props.editMode && props.course) {
			this.state = {
				name: props.course.name,
				description: props.course.description,
				units: props.course.units,
				tags: props.course.tags,
			}
			props.setSelectedPhoto(props.course.unsplashPhoto)
		}
	}

	setName = (name) => {
		this.setState({
			name
		})
	}

	setDescription = (description) => {
		this.setState({
			description
		})
	}

	setUnits = (units) => {
		this.setState({
			units
		})
	}

	setTags = (tags) => {
		this.setState({
			tags
		})
	}

	createCourse = (e) => {
		e.preventDefault()
		this.props.createCourse({
			name: this.state.name,
			description: this.state.description,
			units: this.state.units,
			unsplashImageId: this.props.selectedPhoto ? this.props.selectedPhoto.id : null,
			tags: this.state.tags,
		})
	}

	updateCourse = (e) => {
		e.preventDefault()
		this.props.updateCourse({
			slug: this.props.course.slug,
			name: this.state.name,
			description: this.state.description,
			units: this.state.units,
			unsplashImageId: this.props.selectedPhoto ? this.props.selectedPhoto.id : null,
			tags: this.state.tags,
		})
	}

	render = () => {
		return (
			<Form
				name={this.state.name}
				setName={this.setName}
				description={this.state.description}
				setDescription={this.setDescription}
				units={this.state.units}
				tags={this.state.tags}
				setUnits={this.setUnits}
				setTags={this.setTags}
				onSubmit={this.props.editMode ? this.updateCourse : this.createCourse}
				buttonText={this.props.editMode ? 'Update Course' : 'Create Course'}
				allTags={this.props.allTags} />
		)
	}

}

const mapStateToProps = state => ({
	selectedPhoto: state.unsplash.selectedPhoto,
})

const mapDispatchToProps = ({ course: { createCourse, updateCourse }, unsplash: { setSelectedPhoto } }) => ({
	createCourse: (name, description, unsplashImageId, units) => createCourse(name, description, unsplashImageId, units),
	updateCourse: (slug, name, description, unsplashImageId, units) => updateCourse(slug, name, description, unsplashImageId, units),
	setSelectedPhoto: (selectedPhotoId) => setSelectedPhoto(selectedPhotoId),
})

export default connect(mapStateToProps, mapDispatchToProps)(CourseCreateForm);
