import { Button, ExpansionPanelActions, TextField, Typography } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import dynamic from "next/dynamic";
import React from 'react';
import { connect } from 'react-redux';
import DeleteDialog from './DeleteDialog';

const DynamicTextEditor = dynamic(() => import("./TextEditor"), { ssr: false }); <DynamicTextEditor />

const useStyles = makeStyles(theme => ({
	root: {
		width: '100%',
		padding: '20px 15px 20px 15px',
	},
	title: {
		marginBottom: '30px',
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary,
	},
	icon: {
		verticalAlign: 'bottom',
		height: 20,
		width: 20,
	},
	details: {
		alignItems: 'center',
	},
	columnSmall: {
		flexBasis: '33.333%',
	},
	columnLarge: {
		flexBasis: '66.666%',
	},
	helper: {
		borderLeft: `2px solid ${theme.palette.divider}`,
		padding: theme.spacing(1, 2),
	},
	link: {
		color: theme.palette.primary.main,
		textDecoration: 'none',
		'&:hover': {
			textDecoration: 'underline',
		},
	},
	newAction: {
		display: 'block',
		marginTop: '20px',
	},
	grow: {
		flexGrow: 1,
	},
	nameTextField: {
		width: '100%',
	},
	quillContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		maxWidth: '100%',
		minWidth: '300px',
		alignContent: 'center',
		alignItems: 'center',
	},
	expansionPanel: {
		width: '100%',
	},
	goalExpansionPanel: {
		width: '100%',
		backgroundColor: '#fbfbfb',
	},
}));

function CreateUnitForm({ units, setUnits }) {
	function addEmptyUnit() {
		let currentUnits = units
		currentUnits.push({
			name: `Unit ${currentUnits.length + 1}`,
			description: "",
			goals: [],
			index: currentUnits.length + 1,
		})
		setUnits([...currentUnits])
	}

	function updateUnit(unitIndex, unit) {
		if (unitIndex >= 0 && unitIndex < units.length) {
			Object.assign(units[unitIndex], unit)
			setUnits([...units])
		}
	}

	function deleteUnit(unitIndex) {
		if (unitIndex >= 0 && unitIndex < units.length) {
			units.splice(unitIndex, 1)
			// Readjust the indexes from the unit that was deleted to the end of the list
			for (let i = unitIndex; i < units.length; ++i) {
				units[i].index = i
			}
			setUnits([...units])
		}
	}

	function addEmptyGoal(unitIndex) {
		if (unitIndex >= 0 && unitIndex < units.length) {
			let unitToUpdate = units[unitIndex]
			unitToUpdate.goals.push({
				name: `Goal ${unitToUpdate.goals.length + 1}`,
				description: "",
				index: unitToUpdate.goals.length + 1,
			})
			updateUnit(unitIndex, unitToUpdate)
		}
	}

	function updateGoal(unitIndex, goalIndex, goal) {
		if (unitIndex < 0 || unitIndex >= units.length || goalIndex < 0 || goalIndex >= units[unitIndex].goals.length) {
			return
		}
		let unitToUpdate = units[unitIndex]
		Object.assign(unitToUpdate.goals[goalIndex], goal)
		updateUnit(unitIndex, unitToUpdate)
	}

	function deleteGoal(unitIndex, goalIndex) {
		if (unitIndex < 0 || unitIndex >= units.length || goalIndex < 0 || goalIndex >= units[unitIndex].goals.length) {
			return
		}
		let unitToUpdate = units[unitIndex]
		unitToUpdate.goals.splice(goalIndex, 1)
		// Readjust the indexes from the goal that was deleted to the end of the list
		for (let i = goalIndex; i < unitToUpdate.goals.length; ++i) {
			unitToUpdate.goals[i].index = i
		}
		updateUnit(unitIndex, unitToUpdate)
	}


	const classes = useStyles();
	return (
		<div className={classes.root}>
			<Typography className={classes.title} variant='h5'>Units and Goals</Typography>
			{units.map((unit, unitIndex) => {
				return (
					<ExpansionPanel defaultExpanded className={classes.expansionPanel} key={`unit-${unitIndex}`}>
						<ExpansionPanelSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1c-content"
							id="panel1c-header"
						>
							<div className={classes.columnSmall}>
								<Typography className={classes.heading}>{unit.name}</Typography>
							</div>
						</ExpansionPanelSummary>
						<ExpansionPanelDetails className={classes.details}>
							<div className={classes.quillContainer}>
								<TextField
									autoFocus
									required
									inputProps={{
										maxLength: 30,
									}}
									id={`unit-${unitIndex}-name`}
									label="Unit Name"
									className={classes.nameTextField}
									value={unit.name}
									onChange={e => {
										updateUnit(unitIndex, {
											name: e.target.value,
										})
									}}
									margin="normal"
								/>
								<DynamicTextEditor maxLength={4000} id={`unit-${unitIndex}-description`} height="200px" theme='snow' value={unit.description} placeholder="Description" onChange={(value) => {
									updateUnit(unitIndex, {
										description: value,
									})
								}} />
								{unit.goals.map((goal, goalIndex) => {
									return (
										<ExpansionPanel defaultExpanded className={classes.goalExpansionPanel} key={`goal-${unitIndex}-${goalIndex}`}>
											<ExpansionPanelSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1c-content"
												id="panel1c-header"
											>
												<div className={classes.columnSmall}>
													<Typography className={classes.heading}>{goal.name}</Typography>
												</div>
											</ExpansionPanelSummary>
											<ExpansionPanelDetails className={classes.details}>
												<div className={classes.quillContainer}>
													<TextField
														autoFocus
														required
														inputProps={{
															maxLength: 30,
														}}
														id={`goal-${unitIndex}-${goalIndex}-name`}
														label="Goal Name"
														className={classes.nameTextField}
														value={goal.name}
														onChange={e => {
															updateGoal(unitIndex, goalIndex, {
																name: e.target.value,
															})
														}}
														margin="normal"
													/>
													<DynamicTextEditor maxLength={4000} id={`goal-${unitIndex}-${goalIndex}`} height="200px" theme='snow' value={goal.description} placeholder="Description" onChange={(value) => {
														updateGoal(unitIndex, goalIndex, {
															description: value,
														})
													}} />
												</div>
											</ExpansionPanelDetails>
											<ExpansionPanelActions>
												<DeleteDialog
													buttonText="Delete Goal"
													title="Delete Goal"
													content={`Are you sure you want to delete ${goal.name}?`}
													onDelete={() => {
														deleteGoal(unitIndex, goalIndex)
													}}
													ariaLabel={`delete-goal-${unitIndex}-${goalIndex}`}
												/>
											</ExpansionPanelActions>
										</ExpansionPanel>
									)
								})}
							</div>
						</ExpansionPanelDetails>
						<ExpansionPanelActions>
							<Button color='primary' variant='outlined' onClick={() => {
								addEmptyGoal(unitIndex)
							}}>New Goal</Button>
							<DeleteDialog
								buttonText="Delete Unit"
								title="Delete Unit"
								content={`Are you sure you want to delete ${unit.name}?`}
								onDelete={() => {
									deleteUnit(unitIndex)
								}}
								ariaLabel={`delete-unit-${unitIndex}`}
							/>
						</ExpansionPanelActions>
					</ExpansionPanel>
				)
			})}
			<Button className={classes.newAction} color='primary' variant='outlined' onClick={() => {
				addEmptyUnit()
			}}>New Unit</Button>
		</div>
	);
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = () => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateUnitForm);
