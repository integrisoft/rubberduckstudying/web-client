import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		backgroundColor: '#2e282a',
		color: '#eeeeee',
	},
	brandLink: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
}));

// class NavBar extends Component {
export default ({ dark, small }) => {
	const classes = useStyles();
	// const { loggedIn } = props
	return (
		<Link href="/" className={classes.brandLink}>
			<img
				src={dark ? '/static/images/horizontal-logo.svg' : '/static/images/horizontal-logo-inverted.svg'}
				alt="RubberDuckStudying"
				height={dark ? small ? '24px' : '28px' : small ? '24px' : '28px'}
			/>
		</Link>
	)
}
