import { Button, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		padding: '50px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		backgroundColor: '#f8f8f9',
		textAlign: 'center',
	},
	title: {
		opacity: 0.9,
		marginBottom: '30px',
	}

}));

export default function FooterSignUp() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Typography variant='h6' className={classes.title}>The world's knowledge is at your fingertips. Sign up for free today!</Typography>
			<Link href="/signup" className={classes.button}>
				<Button variant="contained" color="primary" className={classes.button}>Sign Up</Button>
			</Link>
		</div >
	);
}
