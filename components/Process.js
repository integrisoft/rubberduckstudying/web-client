import { Button, Container, Step, StepButton, Stepper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		padding: '50px',
		display: 'flex',
		// flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	processContainer: {
		display: 'flex',
		// flexWrap: 'wrap',
		flexDirection: 'row',
		alignContent: 'center',
		[theme.breakpoints.down('md')]: {
			flexDirection: 'column',
		},
		justifyContent: 'center',
		alignItems: 'center',
		// margin: '0',
	},
	stepContentContainer: {
		width: '315px',
	},
	titleText: {
		color: theme.palette.text.primary,
		opacity: '0.9',
		marginBottom: '20px',
		fontWeight: 500,
	},
	stepContent: {
		padding: '24px',
		lineHeight: '1.9',
		textAlign: 'center',
	},
	stepButton: {
		textAlign: 'left',
	}
}));

function getSteps() {
	return [
		'Create your account',
		'Tell us what you want to learn',
		'Setup a structured course',
		'Find free online resources',
		'Study',
		'Create a tutorial for what you just learned',
	];
}

function getStepContent(step) {
	switch (step) {
		case 0:
			return (
				<>
					<Link href="/signup">
						<Button variant="contained" color="primary">Create my account</Button>
					</Link>
				</>
			);
		case 1:
			return `After creating your account, search our available courses for a topic you might be interested in. If a course doesn't exist yet, use one of our guides to setup a new course for you and other like-minded students.`;
		case 2:
			return `Use our guides to find existing course structures online. This will help provide you with a focused learning experience with clear goals and milestones.`;
		case 3:
			return `After setting up your course, we will assist you in finding free online resources to bootstrap the learning process.
			These resources can include video tutorials, articles, or even tutorials on this site written by people just like you!`;
		case 4:
			return `Use the resources found above to gain a baseline level understanding of the topic. After you feel like you are starting to understand the material, continue onto the next step.`;
		case 5:
			return `Reinforce what you learned on you own by creating a tutorial without using any notes if possible.
			Imagine you were explaining the topic to someone who had the same knowledge that you had before completing the previous step. This will allow you to see
			areas that need improvement and also the other areas that you are proficient in.`;
		default:
			return 'Unknown step';
	}
}

export default function Process() {
	const classes = useStyles();
	const [activeStep, setActiveStep] = React.useState(0);
	const steps = getSteps()

	function handleNext() {
		setActiveStep(prevActiveStep => prevActiveStep + 1);
	}

	function handleBack() {
		setActiveStep(prevActiveStep => prevActiveStep - 1);
	}

	function handleReset() {
		setActiveStep(0);
	}

	const handleStep = step => () => {
		setActiveStep(step);
	};

	return (
		<div className={classes.root}>
			<Typography variant='h5' className={classes.titleText}>How Does it Work?</Typography>
			<Container maxWidth='lg' className={classes.processContainer}>
				<Stepper activeStep={activeStep} nonLinear orientation="vertical">
					{steps.map((label, index) => (
						<Step key={label}>
							<StepButton
								className={classes.stepButton}
								onClick={handleStep(index)}
							>
								{label}
							</StepButton>
						</Step>
					))}
				</Stepper>
				<div className={classes.stepContentContainer}>
					<Typography className={classes.stepContent}>{getStepContent(activeStep)}</Typography>
				</div>
			</Container>


		</div >
	);
}