import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';

const useStyles = makeStyles(theme => ({
	progress: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
}));

export default function PageSpinner() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			{/* <CircularProgress className={classes.progress} /> */}
			<CircularProgress className={classes.progress} color="secondary" />
		</div>
	);
}