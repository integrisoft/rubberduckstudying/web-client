import { InputBase, Paper } from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from "@material-ui/styles";
import { useState } from "react";

const useStyles = makeStyles(theme => ({
	searchRoot: {
		padding: '2px 4px',
		display: 'flex',
		alignItems: 'center',
		width: '100%',
	},
	input: {
		marginLeft: theme.spacing(1),
		flex: 1,
	},
}));

export default ({ initialSearchQuery, setSearchQuery, placeHolder }) => {
	const classes = useStyles()
	let [typingTimeout, setTypingTimeout] = useState(0);
	let [query, setQuery] = useState(initialSearchQuery);
	return (
		<Paper className={classes.searchRoot}>
			<SearchIcon />
			<InputBase
				className={classes.input}
				placeholder={placeHolder}
				value={query ? query : ""}
				inputProps={{ 'aria-label': { placeHolder } }}
				onChange={(e) => {
					const searchText = e.target.value; // this is the search text
					setQuery(searchText)
					console.log(typingTimeout)
					if (typingTimeout) {
						console.log('Clearing timeout')
						clearTimeout(typingTimeout);
					}
					typingTimeout = setTimeout(() => {
						console.log('Setting real search query')
						setSearchQuery(searchText)
					}, 700);
				}}
			/>
		</Paper>
	)
}
