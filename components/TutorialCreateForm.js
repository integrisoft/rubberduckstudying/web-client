import { Breadcrumbs, Button, Container, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import dynamic from "next/dynamic";
import { connect } from 'react-redux';
import Link from './Link';
import ResourceDrawer from './ResourceDrawer';
const DynamicTextEditor = dynamic(() => import("./TextEditor"), { ssr: false }); <DynamicTextEditor />

const useStyles = makeStyles(theme => ({
	root: {
		padding: '30px 10px 0px 10px',
		display: 'flex',
		// flexWrap: 'wrap',
		height: '100%',
		flexDirection: 'row',
		// alignContent: 'center',
		// alignItems: 'center',
	},
	// root: {
	// 	// textAlign: 'center',
	// 	// display: 'flex',
	// 	// flexWrap: 'wrap',
	// 	// flexDirection: 'column',
	// 	// alignContent: 'center',
	// 	// alignItems: 'center',
	// 	width: '100%',

	// },
	breadCrumbNav: {
		marginBottom: '50px',
	},
	content: {
		display: 'flex',
		flexGrow: 1,
		padding: theme.spacing(3),
	},
	form: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		width: '100%',
		minWidth: '300px',
		alignContent: 'center',
		alignItems: 'center',
		marginTop: '50px',
	},
	textField: {
		width: '100%',
	},
	button: {
		marginTop: '30px',
		marginRight: '30px',
		// width: '300px',
	},
}));

function BreadcrumbNav(props) {
	const classes = useStyles();
	return (
		<Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumbNav}>
			<Link color="inherit" href={`/course/${props.goal.parentUnit.parentCourse.slug}`}>
				{props.goal.parentUnit.parentCourse.name}
			</Link>
			<Typography color='textPrimary'>{props.goal.parentUnit.name}</Typography>
			<Link color="inherit" href={`/course/${props.goal.parentUnit.parentCourse.slug}/${props.goal.parentUnit.slug}/${props.goal.slug}`}>
				{props.goal.name}
			</Link>
			{props.editMode ?
				<>
					<span>
						<Typography component='span' color='textPrimary'>{props.tutorial.title}</Typography>
					</span>
				</> : ""
			}
		</Breadcrumbs>
	)
	// }
	// return (
	// 	""
	// )
}

function Form({ title, setTitle, content, setContent, onSubmit, buttonText, previewURL, editMode, goal, tutorial }) {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<main className={classes.content}>
				<Container maxWidth="lg" >
					<BreadcrumbNav editMode={editMode} goal={goal} tutorial={tutorial} />
					<form className={classes.form} onSubmit={onSubmit}>
						<div>
							<Button variant="contained" color='primary' type='submit' className={classes.button}>{buttonText}</Button>
							{editMode ?
								<Link href={`/course/${goal.parentUnit.parentCourse.slug}/${goal.parentUnit.slug}/${goal.slug}/tutorial/${tutorial.id}`}>
									<Button variant='contained' color='default' className={classes.button}> Cancel</Button>
								</Link> : ""
							}
						</div>
						<TextField
							required
							id="title"
							label="Title"
							className={classes.textField}
							value={title}
							onChange={e => setTitle(e.target.value)}
							margin="normal"
							inputProps={{
								maxLength: 50,
							}}

						/>
						<DynamicTextEditor maxLength={100000} id="tutorialContent" height="1500px" theme="snow" value={content} placeholder="Write your tutorial here!" onChange={(value) => { setContent(value) }} />
					</form>
				</Container>
			</main>
			<ResourceDrawer />
		</div>
	)
}

class TutorialCreateForm extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			title: "",
			content: "",
		}

		if (props.editMode && props.tutorial) {
			this.state = {
				title: props.tutorial.title,
				content: props.tutorial.content,
			}
		}
	}

	setTitle = (title) => {
		this.setState({
			title
		})
	}

	setContent = (content) => {
		this.setState({
			content
		})
	}

	createTutorial = (e) => {
		e.preventDefault()
		this.props.createTutorial({
			title: this.state.title,
			content: this.state.content,
			goal: this.props.goal,
		})
	}

	updateTutorial = (e) => {
		e.preventDefault()
		this.props.updateTutorial({
			title: this.state.title,
			content: this.state.content,
			tutorialID: this.props.tutorial.id,
		})
	}

	render = () => {
		return (
			<>
				<Form
					title={this.state.title}
					setTitle={this.setTitle}
					content={this.state.content}
					setContent={this.setContent}
					onSubmit={this.props.editMode ? this.updateTutorial : this.createTutorial}
					buttonText={this.props.editMode ? 'Update Tutorial' : 'Create Tutorial'}
					editMode={this.props.editMode}
					tutorial={this.props.tutorial}
					goal={this.props.goal}
				// previewURL={`/course/${this.props.goal.parentUnit.parentCourse.slug}/${this.props.goal.parentUnit.slug}/${this.props.goal.slug}/tutorial/${this.props.tutorial.id}`}
				/>
			</>
		)
	}

}

const mapStateToProps = state => ({
})

const mapDispatchToProps = ({ tutorial: { createTutorial, updateTutorial } }) => ({
	createTutorial: (title, content, goalID) => createTutorial(title, content, goalID),
	updateTutorial: (title, content, tutorialID) => updateTutorial(title, content, tutorialID),
})

export default connect(mapStateToProps, mapDispatchToProps)(TutorialCreateForm);
