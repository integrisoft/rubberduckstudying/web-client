import { CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(them => ({
	root: {
		display: 'flex',
		// alignContent: 'center',
		alignItems: 'center',
		flexGrow: 1,
		textAlign: 'center',
		height: '100vh',
	},
	loading: {
		margin: '0 auto',
	},
}))
export default () => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<CircularProgress className={classes.loading} size={40} />
			{/* <LinearProgress className={classes.loading} variant="query"  /> */}
		</div>
	)
}