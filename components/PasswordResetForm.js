import { Paper, TextField, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/styles';
import { useState } from "react";
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '50px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	form: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		width: '80%',
	},
	textField: {
		maxWidth: '100%',
	},
	button: {
		marginTop: '20px',
		marginBottom: '30px',
	},
	title: {
		marginBottom: '10px',
	},
	subTitle: {
		opacity: '0.7',
	},
}));

function PasswordResetForm({ resetPassword, token }) {

	const classes = useStyles();
	const [newPassword, setNewPassword] = useState("");
	const [passwordError, setPasswordError] = useState(false);
	const [passwordHelperText, setPasswordHelperText] = useState("");

	function handleSubmit(e) {
		e.preventDefault()
		resetPassword({ token, newPassword })
	}

	function onPasswordChange(e) {
		setNewPassword(e.target.value)
		if (event.target.value.length == 0 || event.target.value.length >= 8) {
			setPasswordError(false)
			setPasswordHelperText('')
		} else {
			setPasswordError(true)
			setPasswordHelperText('Password must be a minimum of 8 characters')
		}
	}

	return (
		<Paper className={classes.root}>
			<form className={classes.form} onSubmit={handleSubmit}>
				<Typography variant='h5' className={classes.title}>Reset Password</Typography>
				<Typography variant='subtitle1' className={classes.subTitle}>Password must be at least 8 characters long.</Typography>
				<TextField
					autoFocus
					required
					id="newPassword"
					label="New Password"
					type="password"
					// placeholder="User"
					className={classes.textField}
					value={newPassword}
					error={passwordError}
					helperText={passwordHelperText}
					onChange={onPasswordChange}
					margin="normal"
				/>
				<Button variant="contained" color='primary' type='submit' className={classes.button}>Request Password Reset</Button>
			</form>
		</Paper>
	)

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = ({ user: { resetPassword } }) => ({
	resetPassword: (token, newPassword) => resetPassword(token, newPassword),
})

export default connect(mapStateToProps, mapDispatchToProps)(PasswordResetForm);
