import { Button, IconButton, Modal, Paper, Typography } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';
import UnsplashPhotoList from './UnsplashPhotoList';

const useStyles = makeStyles(theme => ({
	courseBanner: {
		// width: '100%',
		height: 'auto',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		marginBottom: '5x',
		maxHeight: '680px',
		maxWidth: '680px',
	},
	courseBannerImg: {
		width: '100%',
		maxHeight: '680px',
		maxWidth: '680px',
	},
	paper: {
		position: 'absolute',
		// width: 400,
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		// padding: theme.spacing(2, 4, 3),
	},
	attribution: {
		marginTop: '10px',
		color: '#333',
		marginBottom: '15px',
		opacity: '0.7',
		fontSize: '12px',
		textDecoration: 'none',
	},
	attributionLight: {
		marginTop: '10px',
		color: '#fff',
		marginBottom: '15px',
		opacity: '0.7',
		fontSize: '12px',
		textDecoration: 'none',
	},
	attributionLink: {
		color: '#333',
		marginBottom: '15px',
		opacity: '0.7',
		fontSize: '12px',
		textDecoration: 'none',
	},
	attributionLinkLight: {
		color: '#fff',
		marginBottom: '15px',
		opacity: '0.7',
		fontSize: '12px',
		textDecoration: 'none',
		'&:hover': {
			opacity: '0.9',
		}
	},
	actionButton: {
		margin: '10px 0px 20px 0px',
	},
}));

function getModalStyle() {
	const top = 50;
	const left = 50;

	return {
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
	};
}

function CourseImageViewer({ captionColor, editMode, selectedPhoto }) {
	const classes = useStyles();
	const theme = useTheme();
	const largeWindow = useMediaQuery(theme.breakpoints.up('md'));

	// getModalStyle is not a pure function, we roll the style only on the first render
	const [modalStyle] = React.useState(getModalStyle);
	const [open, setOpen] = React.useState(false);

	return (
		<>
			<Paper
				className={classes.courseBanner}>
				<img className={classes.courseBannerImg} src={selectedPhoto ?
					largeWindow ?
						`${selectedPhoto.images.small}` :
						`${selectedPhoto.images.thumb}`
					: `/static/images/default-course-img.svg`
				} style={!largeWindow && !selectedPhoto ? { height: '400px', width: '400px' } : {}} />
			</Paper >
			{selectedPhoto ?
				<Typography className={captionColor == 'light' ? classes.attributionLight : classes.attribution} variant='caption'>Photo by <a className={captionColor == 'light' ? classes.attributionLinkLight : classes.attributionLink} href={`${selectedPhoto.user.links.html}?utm_source=rubberduckstudying&utm_medium=referral`}>
					{selectedPhoto.user.name}
				</a> on <a className={captionColor == 'light' ? classes.attributionLinkLight : classes.attributionLink} href="https://unsplash.com/?utm_source=rubberduckstudying&utm_medium=referral">
						Unsplash
					</a>
				</Typography> : ""
			}
			{editMode ?
				<>
					<Modal
						aria-labelledby="simple-modal-title"
						aria-describedby="simple-modal-description"
						open={open}
						onClose={() => setOpen(false)}
					>
						<div style={modalStyle} className={classes.paper}>
							<IconButton aria-label="Close Image Selection" onClick={() => setOpen(false)}>
								<CloseIcon />
							</IconButton>
							{/* <h2 id="simple-modal-title">Text in a modal</h2> */}
							<UnsplashPhotoList style={modalStyle} />
							{/* <p id="simple-modal-description">
							Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
						</p> */}
						</div>
					</Modal>
					<Button className={classes.actionButton} onClick={() => setOpen(true)} color='default' variant='contained'>Change Banner</Button>
				</>
				: ""
			}
		</>
	)

}

const mapStateToProps = state => ({
	selectedPhoto: state.unsplash.selectedPhoto,
})

const mapDispatchToProps = () => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(CourseImageViewer);
