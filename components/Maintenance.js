import { Container, Typography } from "@material-ui/core";

export function Maintenance(props) {
	return (
		<div style={{
			backgroundColor: '#f8f8f9',
			height: '100%',
		}}>
			<Container maxSize='md' style={{
				display: 'flex',
				flexWrap: 'wrap',
				flexDirection: 'column',
				alignContent: 'center',
				alignItems: 'center',
				textAlign: 'center',
				padding: '100px 50px',
			}}>
				<img
					src={'/static/images/undraw_maintenance_cn7j.svg'}
					alt="Maintenance"
					height={'256px'}
					style={{
						marginBottom: '50px',
					}} />
				<Typography variant='h5'>Temporarily down for maintenance</Typography>
				<Typography variant='h5' style={{
					marginBottom: '50px',
				}}>We’ll be back soon!</Typography>
				<Typography variant='h6' style={{
					marginBottom: '20px',
				}}>
					Sorry for the inconvenience but we’re performing some maintenance at the moment.
													we’ll be back online shortly!
				</Typography>
				<Typography variant='h6'>— The RubberDuckStudying Team</Typography>
				<img
					src={'/static/images/horizontal-logo.svg'}
					alt="RubberDuckStudying"
					height={'32px'}
					style={{
						marginTop: '50px',
					}} />
			</Container >
		</div>
	)
}