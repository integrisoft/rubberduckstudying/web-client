import InternalServerError from './InternalServerError';
import NotFound from './NotFound';
import Unauthorized from './Unauthorized';

export default ({ error }) => {
	console.log('Error handler ', error)
	if (error.GraphQlErrors && error.GraphQlErrors[0] && error.GraphQlErrors[0].extensions) {
		code = error.GraphQlErrors[0].extensions.code
		message = error.GraphQlErrors[0].extensions.message
		if (code == 404) {
			return <NotFound />
		} else if (code == 401) {
			return <Unauthorized />
		}
		return (
			<InternalServerError error={error} />
		)
	}

	return (
		<InternalServerError error={error} />
	)
}