import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import gql from 'graphql-tag';
import { useState } from 'react';
import { useQuery } from 'react-apollo';
import { connect } from 'react-redux';
import { photoFields } from '../shared/models/schemaFields';
import CourseList from './CourseList';
import EmptyCourseList from './EmptyCourseList';
import Link from './Link';
import Loading from './Loading';
import SearchBar from './SearchBar';

const GET_COURSES = gql`
  query courses($orderBy: String, $offset: Int, $limit: Int, $search: String) {
    courses(orderBy: $orderBy, offset: $offset, limit: $limit, search: $search) {
			id
			slug
			name
			description
			unsplashImageId
			unsplashPhoto {
				${photoFields.join(',')}
			}
			tags
			users {
				id
			}
			units {
				id
				slug
				goals {
					id
					slug
					tutorials {
						id
					}
				}
			}
    }
  }
`;

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		width: '100%',
	},
	title: {
		marginTop: '20px',
		marginBottom: '20px',
	},
	courseListContainer: {
		margin: '30px 0',
	},
	headerContainer: {
		display: 'flex',
		alignItems: 'center',
		marginBottom: '40px'
	},
	searchContainer: {
		width: '50%',
		display: 'inline-block',
		minWidth: '310px',
		marginRight: '20px',
	},
}));

function CourseBrowser(props) {
	const classes = useStyles()
	const [numCoursesInLastFetch, setNumCoursesInLastFetch] = useState(-1)
	const [searchQuery, setSearchQuery] = useState(props.initialSearchQuery)
	const { data, error, fetchMore, networkStatus } = useQuery(GET_COURSES, {
		variables: {
			orderBy: "updated_at desc",
			offset: 0,
			limit: 20,
			search: searchQuery
		},
		notifyOnNetworkStatusChange: true,
		fetchPolicy: 'network-only'
	});
	if (error) {
		return ""
	}
	if (!data || !data.courses) {
		return <Loading />;
	}

	return (
		<div>
			<div className={classes.headerContainer}>
				<div className={classes.searchContainer}>
					<SearchBar placeHolder={'Search Courses'} initialSearchQuery={props.initialSearchQuery} setSearchQuery={(query) => {
						console.log('Searching for ', query)
						setSearchQuery(query)
					}} />
				</div>
				<Link href='/course/create'>
					<Button color='primary' variant='contained'>New Course</Button>
				</Link>
			</div>

			<div className={classes.courseListContainer}>
				{data.courses.length == 0 ?
					<EmptyCourseList message='No courses found. Why not create one?' create={true} browse={false} />
					:
					<CourseList courses={data.courses} numCoursesInLastFetch={numCoursesInLastFetch} fetchMore={fetchMore} updateQuery={(prev, fetchMoreResult) => {
						setNumCoursesInLastFetch(fetchMoreResult.courses.length)
						return Object.assign({}, prev, {
							courses: [...prev.courses, ...fetchMoreResult.courses]
						});
					}} networkStatus={networkStatus} perPage={20} />
				}
			</div>
		</div >
	)
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = () => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(CourseBrowser)