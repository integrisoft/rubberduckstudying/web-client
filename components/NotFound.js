import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Link from "./Link";

const useStyles = makeStyles(theme => ({
	root: {
		padding: '200px 20px 200px 20px',
		flexGrow: 1,
		textAlign: 'center',
	},
}))

export default () => {
	const classes = useStyles()
	return (
		<Container className={classes.root} maxWidth='md'>
			<img
				src={'/static/images/undraw_void.svg'}
				alt="Maintenance"
				height={'256px'}
				style={{
					marginBottom: '50px',
				}} />
			<Typography variant='h6'>Sorry, we couldn't find anything on this page. <Link href='/'>Return Home</Link>.</Typography>
			<img
				src={'/static/images/horizontal-logo.svg'}
				alt="RubberDuckStudying"
				height={'32px'}
				style={{
					marginTop: '30px',
				}} />
		</Container>
	)
}