import { Avatar, Card, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import { formatUtcDate } from '../shared/dateformat';
import CourseList from './CourseList';

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: '20px',
		flexGrow: 1,
		marginBottom: '30px',
		padding: '50px 20px',
		display: 'flex',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	profileAvatar: {
		height: '100px',
		width: '100px',
		fontSize: '36px',
		marginBottom: '50px',
		backgroundColor: theme.palette.primary.main,
	},
	profileSectionTitle: {
		marginBottom: '20px',
	},
	profileSection: {
		marginTop: '20px',
		padding: '50px 20px',
		display: 'flex',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	courseListContainer: {
		margin: '30px 0',
		// width: '100%',
		maxHeight: '2000px',
		overflowY: 'auto',
		overflowX: 'hidden',
	},
}))

const UserViewer = ({ user }) => {
	const classes = useStyles();

	return (
		<>
			<Card className={classes.root}>
				<Avatar className={classes.profileAvatar}>{user.username[0].toUpperCase()}</Avatar>
				<Typography variant='h5'>{user.username}</Typography>
				<Typography variant='body1'>Member since {formatUtcDate(user.createdAt)}</Typography>
			</Card>
			<section className={classes.profileSection}>
				<Typography variant='h6' className={classes.profileSectionTitle} >Bio</Typography>
				<Typography variant='body1'>{user.bio}</Typography>
			</section>
			<section className={classes.profileSection}>
				<Typography variant='h6' className={classes.profileSectionTitle} >Courses</Typography>
				<div className={classes.courseListContainer}>
					<CourseList courses={user.courses} />
				</div>
			</section>
		</>
	)
}

export default UserViewer