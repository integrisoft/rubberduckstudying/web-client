import { Avatar, Card, CardActionArea, CardContent, CardMedia, Grid, List, ListItem, ListItemAvatar, ListItemText, makeStyles, Typography, useMediaQuery, useTheme } from '@material-ui/core';
import Router from 'next/router';
import pluralize from 'pluralize';
import { useState } from 'react';
import { connect } from 'react-redux';
import { Waypoint } from 'react-waypoint';
import Link from './Link';
const uuidv4 = require('uuid/v4');

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignContent: 'center',
		alignItems: 'center',
		marginTop: '50px',
		marginBottom: '50px',
	},
	courseCard: {
		// padding: '20px',
		width: 216,
		height: 290,
	},
	courseName: {
		fontSize: '15px',
		fontWeight: '500',
		marginBottom: '20px',
	},
	media: {
		height: 122,
		// borderBottom: '2px solid #ccc',
		boxShadow: 'rgba(232, 233, 235, 0.5) 0px 1px 0px 0',
		// width: 216,
	},
	cardContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignContent: 'center',
		alignItems: 'center',
	},
	inline: {
		display: 'inline',
	},
	block: {
		display: 'block',
	},
	courseListItem: {
		border: '1px solid #eee',
		marginBottom: '10px',
		boxShadow: 'rgba(232, 233, 235, 0.5) 1px 1px 1px 1px',
		padding: '0 10px 0 0',
		// maxWidth: '375px',

	},
	courseListItemImageContainer: {
		marginTop: 0,
		marginRight: '10px',
	},
	courseListItemImage: {
		width: '80px',
		height: '80px',
		// padding: 0,
	},
}));

function CourseGrid({ courses, fetchMore, numCoursesInLastFetch, updateQuery, networkStatus, perPage }) {
	const classes = useStyles()
	const [currentOffset, setCurrentOffset] = useState(0)
	let nextOffset = currentOffset + perPage
	return (
		<>
			<Grid container spacing={2}>
				<Grid item xs={12}>
					<Grid container spacing={2}>
						{courses.map((course, i) => {
							let totalTutorials = course.units.reduce((totalTutorials, unit) => {
								return totalTutorials + unit.goals.reduce((total, goal) => {
									return total + goal.tutorials.length
								}, 0)
							}, 0)
							let totalGoals = course.units.reduce((totalTutorials, unit) => {
								return totalTutorials + unit.goals.length
							}, 0)
							return (
								<React.Fragment key={course.id}>
									<Grid item className={classes.cardContainer}>
										<Link href={`/course/${course.slug}`} key={course.id}>
											<CardActionArea>
												<Card className={classes.courseCard}>
													<CardMedia
														className={classes.media}
														image={course.unsplashPhoto ? course.unsplashPhoto.images.thumb : "/static/images/default-course-img.svg"}
														title={course.name}
													/>
													<CardContent>
														<Typography noWrap className={classes.courseName} variant="body1" component="h2">
															{course.name}
														</Typography>
														<Typography variant="caption" color="textSecondary" component="p">
															<strong>{course.users.length}</strong> {pluralize("student", course.users.length)}
														</Typography>
														<Typography variant="caption" color="textSecondary" component="p">
															<strong>{course.units.length}</strong> {pluralize("unit", course.units.length)} with <strong>{totalGoals}</strong> {pluralize("goal", totalGoals)}
														</Typography>
														<Typography variant="caption" color="textSecondary" component="p">
															<strong>{totalTutorials}</strong> {pluralize("tutorial", totalTutorials)}
														</Typography>
													</CardContent>
												</Card>
											</CardActionArea>
										</Link>
									</Grid>
									{i === (courses.length * 0.1) && fetchMore && (
										<Waypoint
											onEnter={() => {
												if (numCoursesInLastFetch == -1 || numCoursesInLastFetch == perPage) {
													setCurrentOffset(nextOffset)
													fetchMore({
														variables: {
															offset: nextOffset,
															limit: perPage,
														},
														updateQuery: (prev, { fetchMoreResult }) => {
															if (!fetchMoreResult) return prev;
															return updateQuery(prev, fetchMoreResult);
														}
													})
												}
											}}
										/>
									)}
								</React.Fragment>
							)
						})}
					</Grid>
				</Grid>
			</Grid>
			{/* {networkStatus === 3 && <div><CircularProgress /></div>} */}
		</>
	)
}

function CourseStackedList({ courses, numCoursesInLastFetch, fetchMore, updateQuery, networkStatus, perPage }) {
	const classes = useStyles()
	const [currentOffset, setCurrentOffset] = useState(0)
	let nextOffset = currentOffset + perPage
	return (
		<>
			<List>
				{courses.map((course, i) => {
					let totalTutorials = course.units.reduce((totalTutorials, unit) => {
						return totalTutorials + unit.goals.reduce((total, goal) => {
							return total + goal.tutorials.length
						}, 0)
					}, 0)
					let totalGoals = course.units.reduce((totalTutorials, unit) => {
						return totalTutorials + unit.goals.length
					}, 0)
					return (
						<React.Fragment key={course.id}>
							<ListItem className={classes.courseListItem} button alignItems="flex-start" onClick={() => {
								Router.push(`/course/${course.slug}`)
							}}>
								<ListItemAvatar className={classes.courseListItemImageContainer}>
									<Avatar className={classes.courseListItemImage} style={{ borderRadius: 0 }} alt='' src={course.unsplashPhoto ? course.unsplashPhoto.images.thumb : "/static/images/default-course-img.svg"} />
								</ListItemAvatar>
								<ListItemText
									primary={<Typography noWrap>{course.name}</Typography>}
									secondary={
										<React.Fragment>
											<Typography variant="caption" className={classes.block}>
												<strong>{course.users.length}</strong> {pluralize("user", course.users.length)}
											</Typography>
											<Typography
												component="span"
												variant="caption"
												className={classes.inline}
												// color="textPrimary"
												noWrap
											>
												{/* <Typography variant="caption" color="textSecondary" component="p"> */}
												{/* <span>&#183; </span> */}
												<span><strong>{course.units.length}</strong> {pluralize("unit", course.units.length)} </span>
												<span>&#183; </span>
												<span><strong>{totalGoals}</strong> {pluralize("goal", totalGoals)} </span>
												<span>&#183; </span>
												<span><strong>{totalTutorials}</strong> {pluralize("tutorial", totalTutorials)}</span>
											</Typography>
										</React.Fragment>
									}
								/>
							</ListItem>
							{i === (courses.length * 0.1) && fetchMore && (
								<Waypoint
									onEnter={() => {
										if (numCoursesInLastFetch == -1 || numCoursesInLastFetch == perPage) {
											setCurrentOffset(nextOffset)
											fetchMore({
												variables: {
													offset: nextOffset,
													limit: perPage,
												},
												updateQuery: (prev, { fetchMoreResult }) => {
													if (!fetchMoreResult) return prev;
													return updateQuery(prev, fetchMoreResult);
												}
											})
										}
									}}
								/>
							)}
						</React.Fragment>
					)
				})}
			</List>
			{/* {networkStatus === 3 && <div><CircularProgress /></div>} */}
		</>
	)
}

function CourseList({ courses, fetchMore, numCoursesInLastFetch, updateQuery, networkStatus, perPage }) {

	const theme = useTheme();
	// const smWindow = useMediaQuery(theme.breakpoints.up('xs'));
	const xsWindow = useMediaQuery('(max-width:520px)');
	if (xsWindow) {
		return <CourseStackedList courses={courses} numCoursesInLastFetch={numCoursesInLastFetch} fetchMore={fetchMore} updateQuery={updateQuery} networkStatus={networkStatus} perPage={perPage} />
	}
	return <CourseGrid courses={courses} numCoursesInLastFetch={numCoursesInLastFetch} fetchMore={fetchMore} updateQuery={updateQuery} networkStatus={networkStatus} perPage={perPage} />
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = () => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(CourseList)