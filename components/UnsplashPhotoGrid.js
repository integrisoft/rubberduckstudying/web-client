import { Card, CardActionArea, CardMedia, CircularProgress, Grid, makeStyles, Typography } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import gql from 'graphql-tag';
import { useQuery } from 'react-apollo';
import { connect } from 'react-redux';
import { Waypoint } from 'react-waypoint';
import { photoFields } from '../shared/models/schemaFields';
import { referralParameters } from '../shared/models/unsplash';
const uuidv4 = require('uuid/v4')

const LIST_PHOTOS = gql`
  query unsplashListPhotos($page: Int, $perPage: Int) {
    unsplashListPhotos(page: $page, perPage: $perPage) {
			${photoFields.join(',')}
		}
  }
`

const SEARCH_PHOTOS = gql`
  query unsplashSearchPhotos($query: String!, $page: Int, $perPage: Int) {
    unsplashSearchPhotos(query: $query, page: $page, perPage: $perPage) {
			total
			totalPages
			results {
				${photoFields.join(',')}
			}
		}
  }
`

const useStyles = makeStyles(theme => ({
	root: {
		// width: '100%',
		// display: 'flex',
		// flexWrap: 'wrap',
		width: '100%',
		// flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		overflowY: 'scroll',
		height: '430px',
	},
	photoContainer: {
		alignContent: 'center',
		alignItems: 'center',
		display: 'flex',


		// flexGrow: 0,
	},
	photo: {
		// alignContent: 'center',
		// alignItems: 'center',
		// padding: '20px',
		maxWidth: '100px',
		maxHeight: '100px',
		marginBottom: '10px',
	},
	attributionContainer: {
		visibility: 'hidden',
		backgroundColor: '#333',
		opacity: '0.8',
		width: '100%',
		display: 'inline-block',
		textAlign: 'center',
	},
	attribution: {
		backgroundColor: '#222',
		margin: 0,
		padding: 0,
	},
	attributionLink: {
		color: 'white',
		opacity: '0.9',
		textDecoration: 'none',
		"&:hover": {
			textDecoration: 'underline',
		}
	},
	media: {
		width: '100px',
		height: '100px',
		"&:hover": {
			"& span": {
				visibility: 'visible',
				// backgroundColor: '#0f0',
			}
		}
	},
	selectedIcon: {
		color: 'white',
		margin: 'auto',
	},
	selectedOverlay: {
		visibility: 'hidden',
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		width: '100%',
		height: '100%',
		backgroundColor: '#333',
		opacity: '0.8',
		display: 'flex',
		alignContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
	},
	selectedMedia: {
		width: '100px',
		height: '100px',
		"& *": {
			visibility: 'visible',
		},
	},
}));

function UnsplashPhotoGrid({ page, searchQuery, setSelectedPhoto, selectedPhoto }) {
	const classes = useStyles()
	// const [currentPage, setCurrentPage] = useState(1);

	const { data, error, fetchMore, networkStatus } = useQuery(searchQuery == "" ? LIST_PHOTOS : SEARCH_PHOTOS, {
		variables: {
			query: searchQuery,
			page: 1,
			perPage: 54,
		},
		notifyOnNetworkStatusChange: true
	});
	if (error) {
		return ""
	}
	if (!data || (!data.unsplashListPhotos && !data.unsplashSearchPhotos)) {
		return <CircularProgress />;
	}

	let photos = searchQuery == "" || !data.unsplashSearchPhotos ? data.unsplashListPhotos : data.unsplashSearchPhotos.results
	// Let -1 indicate "always fetch more pages because there is no query"
	let totalPages = searchQuery == "" || !data.unsplashSearchPhotos ? -1 : data.unsplashSearchPhotos.totalPages
	let nextPage = Math.trunc((photos.length / 18) + 3)
	return (
		<div className={classes.root}>
			<Grid container justify='center'>
				{photos.map((photo, i) => {
					return (
						<React.Fragment key={photo.id}>
							<Grid container item xs className={classes.photoContainer} justify='center'>
								<Card className={classes.photo}>
									<CardActionArea>
										<CardMedia
											onClick={() => {
												setSelectedPhoto(photo)
											}}
											className={selectedPhoto && photo.id == selectedPhoto.id ? classes.selectedMedia : classes.media}
											image={photo.images.thumb}
											title={photo.user.name}
										>
											<div className={classes.selectedOverlay}>
												<CheckCircleIcon className={classes.selectedIcon} />
											</div>
											<span className={classes.attributionContainer}>
												<Typography noWrap variant='caption' className={classes.attribution}><a className={classes.attributionLink} href={photo.user.links.html + referralParameters}>{photo.user.name}</a></Typography>
											</span>
										</CardMedia>
									</CardActionArea>
								</Card>
							</Grid>
							{i === photos.length - 18 && (
								<Waypoint
									onEnter={() => {
										if (totalPages == -1 || nextPage <= totalPages) {
											fetchMore({
												variables: {
													page: nextPage,
													perPage: 18,
												},
												updateQuery: (prev, { fetchMoreResult }) => {
													if (!fetchMoreResult) return prev;
													return Object.assign({}, prev, {
														unsplashListPhotos: searchQuery == "" ?
															[...prev.unsplashListPhotos, ...fetchMoreResult.unsplashListPhotos] :
															[...prev.unsplashSearchPhotos.results, ...fetchMoreResult.unsplashSearchPhotos.results]
													});
												}
											})
										}
									}
									}
								/>
							)}
						</React.Fragment>
					)
				})}
			</Grid>
			{networkStatus === 3 && <div><CircularProgress /></div>}
		</div>
	)
}

const mapStateToProps = state => ({
	selectedPhoto: state.unsplash.selectedPhoto,
})

const mapDispatchToProps = ({ unsplash: { setSelectedPhoto } }) => ({
	setSelectedPhoto: (photo) => setSelectedPhoto(photo),
})

export default connect(mapStateToProps, mapDispatchToProps)(UnsplashPhotoGrid)