import { withStyles } from '@material-ui/styles';
import React from 'react';
import ReactGA from 'react-ga';
import { connect } from 'react-redux';
import Footer from './Footer';
// import ScrollToTop from './ScrollToTop';
import { Snackbar } from './SnackBar';

const env = process.env.NODE_ENV || "development";
const devMode = env == "development"

const initGA = () => {
	if (!devMode) {
		console.log('GA init')
		ReactGA.initialize('UA-146898365-1')
	}
}

const logPageView = () => {
	if (!devMode) {
		ReactGA.set({ page: window.location.pathname, anonymizeIp: true })
		ReactGA.pageview(window.location.pathname)
	}
}

const GlobalCss = withStyles(theme => ({
	// @global is handled by jss-plugin-global.
	'@global': {
		'.MuiTypography-h1': {
			fontSize: '32px',
			padding: 0,
			fontWeight: 300,
			lineHeight: '1.3333',
			// opacity: 0.9,
			margin: 0,
		},
		'.MuiTypography-h2': {
			fontSize: '26px',
			lineHeight: '1.3333',
			// fontWeight: 600,
			// opacity: 0.9,
		},
		'.MuiTypography-h3': {
			fontSize: '22px',
			fontWeight: 600,
			lineHeight: '1.25',
			// opacity: 0.9,
		},
		'.MuiTypography-body1': {
			fontSize: '15px',
			lineHeight: '1.5',
			fontWeight: 400,
			opacity: 0.9,
			color: '#393b41',
			textRendering: 'optimizeLegibility',
		},
		'.MuiTypography-subtitle1': {
			fontSize: '14px',
			fontWeight: 400,
			// opacity: 0.9,
		},
		'.MuiTypography-subtitle2': {
			fontSize: '14px',
			fontWeight: 400,
			// opacity: 0.7,
		},
		'.MuiTypography-caption': {
			fontSize: '13px',
			fontWeight: 400,
		},
		'.MuiButton-label': {
			textTransform: 'none',
			fontWeight: 500,
			fontSize: '15px',
			textRendering: 'optimizeLegibility',
		},
		'.MuiStepIcon-root.MuiStepIcon-active': {
			// color: theme.palette.secondary.main,
		},
		'.MuiMenuItem-root': {
			fontFamily: 'Open Sans',
		},
	},
}))(() => null);

// …

class App extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		initGA()
		logPageView()
	}

	render() {
		return (
			<>
				{/* <ScrollToTop /> */}
				<main>

					<GlobalCss />

					{this.props.status ? <Snackbar key={this.props.status.date} status={this.props.status} /> : null}
					{this.props.children}
					<style jsx global>{`
						* {
							font-family: 'Open Sans', 'helvetica neue', Helvetica, Arial, sans-serif;
						}
						html, body {
							margin: 0;
							padding: 0;
							height: 100%;
							text-rendering: optimizeLegibility;
						}
						main {
							min-height: 100vh;
						}
						p {
							line-height: 1.58;
							opacity: 0.9;
							font-size: 18px;
						}
						li {
							font-size: 18px;					
						}
						blockquote {
							overflow-wrap: break-word;
							word-break: break-word;
							opacity: 0.8;
							font-size: 24px;
						}
						pre {
							background-color: #23241f;
							color: #f8f8f2;
							box-sizing: border-box;
							border-radius: 3px;
							font-size: 15px;
							font-family: "Lucida Console", Monaco, monospace !important;
							width: 100%;
							font-weight: 400;
							line-height: 18.4667px;
							letter-spacing: 0.14994px;
							margin: 5px 0px;
							padding: 5px 10px;
							-moz-tab-size: 4;
							white-space: pre-wrap;
							cursor: text;
							overflow: visible;
							overflow-wrap: break-word;
						}
						pre span {
							font-family: "Lucida Console", Monaco, monospace !important;
						}import ScrollToTop from './ScrollToTop';

						.ql-align-right {
							text-align: right;
						}
						.ql-align-center {
							text-align: center;
						}
						.ql-align-justify {
							text-align: justify;
						}
					`}</style>
				</main>
				<Footer />
			</>
		)
	}
}

const mapStateToProps = state => {
	return {
		status: state.statusMessage.status
	}
}

const mapDispatchToProps = () => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(App);