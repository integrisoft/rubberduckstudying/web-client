import { Button, Link, Typography } from '@material-ui/core';
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
	root: {
		// textAlign: 'center',
		// padding: '50px 0px 30px 0px',
		// display: 'flex',
		textAlign: 'center',
		paddingTop: '100px',
	},
	actionContainer: {
		marginTop: '10px',
	},
	action: {
		margin: '20px 10px 0px 10px',
		display: 'inline-block',
	},
}))

export default (props) => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<img
				src={'/static/images/undraw_blank_canvas.svg'}
				alt="Blank Canvas"
				height={'256px'}
				style={{
					marginBottom: '50px',
				}} />
			<Typography variant='h6'>{props.message}</Typography>
			<div className={classes.actionContainer}>
				{props.create ?
					<Link href='/course/create' className={classes.action}><Button color='primary' variant='contained'>Create a Course</Button></Link> :
					""
				}
				{props.browse ?
					<Link href='/courses' className={classes.action}><Button color='secondary' variant='contained'>Browse Courses</Button></Link> :
					""
				}
			</div>
		</div>
	)
}