import { Paper, TextField, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/styles';
import { useState } from "react";
import { connect } from 'react-redux';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '50px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	form: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		width: '80%',
	},
	textField: {
		maxWidth: '100%',
	},
	button: {
		marginTop: '20px',
		marginBottom: '30px',
	}
}));

function LoginForm({ redirectTo, login }) {

	const classes = useStyles();
	const [userID, setUserID] = useState("");
	const [password, setPassword] = useState("");

	function handleSubmit(e) {
		e.preventDefault()
		login({ userID, password, redirectTo })
	}

	return (
		<Paper className={classes.root}>
			<form className={classes.form} onSubmit={handleSubmit}>
				<Typography variant='h5'>Log In</Typography>
				<TextField
					autoFocus
					required
					id="userID"
					label="Email or Username"
					// placeholder="User"
					className={classes.textField}
					value={userID}
					onChange={e => setUserID(e.target.value)}
					margin="normal"
				/>
				<TextField
					required
					id="password"
					label="Password"
					type="password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					className={classes.textField}
					margin="normal"
				/>
				<Button variant="contained" color='primary' type='submit' className={classes.button}>Log in</Button>
				<Typography variant='subtitle1'><Link href='/forgot_password'>Forgot Password?</Link></Typography>
			</form>
		</Paper>
	)

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = ({ user: { login } }) => ({
	login: (userID, password) => login(userID, password),
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
