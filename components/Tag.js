import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    chip: {
        margin: theme.spacing(1.5, 0.25),
    },
}));

export default ({ name }) => {
    const classes = useStyles()
    return (
        <Chip
            tabIndex={-1}
            component='a'
            href={`/courses?query=${name}`}
            clickable
            label={name}
            className={classes.chip}
            color='primary'
            size='small'
        />
    )

}