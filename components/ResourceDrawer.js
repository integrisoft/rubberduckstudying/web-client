import { Drawer, Fab, Hidden, IconButton, Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import AddIcon from '@material-ui/icons/Add';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import { makeStyles } from "@material-ui/styles";
import React from 'react';

const drawerWidth = 300;

const useStyles = makeStyles(theme => ({
	drawer: {
		[theme.breakpoints.up('lg')]: {
			width: drawerWidth,
			flexShrink: 0,
		},
		// height: '100%',
	},
	menuButton: {
		marginRight: theme.spacing(1),
		[theme.breakpoints.up('lg')]: {
			display: 'none',
		},
		borderRadius: '0',
		top: '64px',
		right: '0',
		position: 'absolute',
		padding: '8px',
	},
	drawerPaper: {
		width: drawerWidth,
		top: '64px',
		position: 'absolute',
		borderBottom: '1px solid rgb(0, 0, 0, 0.12)',
	},
	resourcesContainer: {
		padding: '20px 20px',
		display: 'flex',
		alignContent: 'center',
		alignItems: 'center',
	},
	resourcesTitle: {
		flexGrow: 1,
	},
	resourcesAction: {
	},
	buttonLabel: {
		marginRight: '10px',
	},
}))

export default (({ addMode, resources }) => {
	const classes = useStyles()
	const [mobileOpen, setMobileOpen] = React.useState(false);
	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen);
	};

	const drawer = (
		<div>
			<div className={classes.resourcesContainer}>
				<Typography className={classes.resourcesTitle} variant='h6'>Resources</Typography>
				{addMode ?
					<Fab
						size='small'
						color='secondary'
						className={classes.resourcesAction}
						onClick={() => {
							alert("Resource attachments are coming soon")
						}}
					>
						<AddIcon />
					</Fab> : ""
				}
			</div>
			<Divider />
		</div >
	)

	return (
		<>
			<nav className={classes.drawer} aria-label="goal-resources">
				{/* The implementation can be swapped with js to avoid SEO duplication of links. */}
				<Hidden lgUp implementation="css">
					<Drawer
						variant="temporary"
						anchor='right'
						open={mobileOpen}
						onClose={handleDrawerToggle}
						classes={{
							paper: classes.drawerPaper,
						}}
						ModalProps={{
							keepMounted: true, // Better open performance on mobile.
						}}
					>
						{drawer}
					</Drawer>
				</Hidden>
				<Hidden mdDown implementation="css">
					<Drawer
						anchor='right'
						classes={{
							paper: classes.drawerPaper,
						}}
						variant="permanent"
						open
					>
						{drawer}
					</Drawer>
				</Hidden>
			</nav>
			<IconButton
				color="inherit"
				aria-label="open resource list"
				edge="start"
				onClick={handleDrawerToggle}
				className={classes.menuButton}
			>
				<Typography className={classes.buttonLabel} variant='subtitle1'>Resources</Typography>
				<MenuOpenIcon />
			</IconButton>
		</>
	)
})