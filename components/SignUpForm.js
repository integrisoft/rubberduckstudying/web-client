import { Paper, TextField, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/styles';
import flowRight from 'lodash.flowright';
import { useState } from "react";
import { connect } from 'react-redux';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '50px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	form: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		width: '80%',
	},
	textField: {
		maxWidth: '100%',
	},
	button: {
		marginTop: '20px',
		marginBottom: '30px',
	}
}));

function SignUpForm({ createUser, checkUsername, usernameIsUnique }) {

	const classes = useStyles();
	const [email, setEmail] = useState("");

	const [username, setUsername] = useState("");

	const [password, setPassword] = useState("");
	const [passwordError, setPasswordError] = useState(false);
	const [passwordHelperText, setPasswordHelperText] = useState("");

	const [name, setName] = useState("");

	function handleSubmit(e) {
		e.preventDefault()
		if (!usernameIsUnique || passwordError) {
			alert('Please fill out all required fields')
			return
		}
		createUser({ email, username, password, name })
	}

	function onPasswordChange(e) {
		setPassword(e.target.value)
		if (event.target.value.length == 0 || (event.target.value.length >= 8 && event.target.value.length <= 200)) {
			setPasswordError(false)
			setPasswordHelperText('')
		} else {
			setPasswordError(true)
			setPasswordHelperText('Password must be a minimum of 8 characters and a maximum of 200 characters')
		}
	}

	function onUsernameChange(e) {
		setUsername(e.target.value)
		checkUsername(event.target.value)
	}

	return (
		<Paper className={classes.root}>
			<Typography variant='h5'>Create Your Account</Typography>
			<form className={classes.form} onSubmit={handleSubmit}>
				<TextField
					autoFocus
					required
					id="name"
					label="Full Name"
					placeholder="Your name here"
					value={name}
					onChange={e => setName(e.target.value)}
					className={classes.textField}
					margin="normal"
					inputProps={{
						maxLength: 100,
					}}
				/>
				<TextField
					required
					id="username"
					label="Username"
					// placeholder="User"
					helperText={!usernameIsUnique ? 'Username not available' : ''}
					error={!usernameIsUnique}
					className={classes.textField}
					value={username}
					onChange={onUsernameChange}
					margin="normal"
					inputProps={{
						maxLength: 30,
					}}
				/>
				<TextField
					required
					id="email"
					label="Email"
					placeholder="user@example.com"
					className={classes.textField}
					value={email}
					onChange={e => setEmail(e.target.value)}
					margin="normal"
					inputProps={{
						maxLength: 400,
					}}
				/>
				<TextField
					required
					helperText={passwordHelperText}
					id="password"
					label="Password"
					type="password"
					value={password}
					onChange={onPasswordChange}
					error={passwordError}
					placeholder="********"
					className={classes.textField}
					margin="normal"
				/>
				<Button variant="contained" color='primary' type='submit' className={classes.button}>Create my account</Button>
				<Typography variant='caption'>By signing up, you agree to our <Link href='legal/terms'>Term of Service</Link> and <Link href='legal/privacy'>Privacy Policy</Link></Typography>
			</form>
		</Paper>
	)
}

const mapStateToProps = state => ({
	usernameIsUnique: state.signup.usernameIsUnique
})

const mapDispatchToProps = ({ user: { createUser }, signup: { usernameIsUnique } }) => ({
	createUser: (email, username, password, name) => createUser(email, username, password, name),
	checkUsername: (username) => usernameIsUnique(username),
})

export default flowRight(connect(mapStateToProps, mapDispatchToProps))(SignUpForm);
