import { Container, Divider, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import EmailIcon from '@material-ui/icons/Email';
import React from 'react';
import Brand from './Brand';
import Link from './Link';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		color: '#eeeeee',
		// marginTop: '100px',
		// marginBottom: '100px',
		// height: '300px',
		backgroundColor: theme.palette.primary.dark,
		color: theme.palette.text.light,

	},
	container: {
		// color: theme.palette.text.light,
		color: '#eee',

		paddingTop: '100px',
		paddingBottom: '100px',
		// backgroundColor: '#0f0',
	},
	gridColumn: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignItems: 'flex-start',
		alignContent: 'center',
		[theme.breakpoints.down('sm')]: {
			alignItems: 'center',
			alignContent: 'center',
			marginBottom: '20px',
		},
	},
	gridColumnTitle: {
		fontSize: '18px',
		marginBottom: '10px',
		fontSize: '14px',
		color: '#fff',
		opacity: '1!important',
	},
	footerItem: {
		// alignItem: 'center',
		// alignContent: 'center',
		// color: theme.palette.text.light,
		color: '#eee',
	},
	gridItemLink: {
		color: '#eee',
		// color: '#333',
		marginBottom: '15px',
		opacity: '0.7',
		fontSize: '12px',
		textDecoration: 'none',
		'&:hover': {
			textDecoration: 'underline',
		}
	},
	gridContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignItems: 'flex-start',
		alignContent: 'space-between',
	},
	bottomContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignItems: 'center',
		alignContent: 'center',
		paddingTop: '50px',
		paddingBottom: '50px',
		[theme.breakpoints.down('sm')]: {
			alignItems: 'center',
			alignContent: 'center',
			// marginBottom: '40px',
		},
		// backgroundColor: '#f00',	
		// color: theme.palette.text.light,
		color: '#eee',

	},
	bottomContainerItem: {
		margin: '0 30px 0 30px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignItems: 'center',
		alignContent: 'center',
		[theme.breakpoints.down('sm')]: {
			marginBottom: '10px',
		},
	},
	textIcon: {
		verticalAlign: 'text-bottom',
		marginRight: '7px',
		fontSize: '18px',
	},
	footerIcon: {
		verticalAlign: 'text-bottom',
		height: '15px',
		width: '18px',
		marginRight: '7px',
	},
}));

// class NavBar extends Component {
export default ({ loggedIn }) => {
	const classes = useStyles();
	// const { loggedIn } = props
	return (
		<div className={classes.root}>
			<Divider variant='fullWidth' />
			<Container maxWidth='lg' className={classes.container} >
				<Grid container spacing={0}>
					<Grid container item item spacing={3} direction='row' justify='space-around' className={classes.gridContainer}>
						<Grid item sm container spacing={1} direction="column" className={classes.gridColumn} >
							<Grid item>
								<Brand />
							</Grid>
						</Grid>
						<Grid item sm container spacing={1} direction="column" className={classes.gridColumn} >
							<Grid item>
								<Typography variant='subtitle1' className={classes.gridColumnTitle}>Explore</Typography>
							</Grid>
							<Grid item className={classes.footerItem}>
								<Link href="/about" className={classes.gridItemLink}>About Us</Link>
							</Grid>
							<Grid item className={classes.footerItem}>
								<a href="https://blog.integrisofthq.com" className={classes.gridItemLink}>Blog</a>
							</Grid>
							<Grid item className={classes.footerItem}>
								<a href="https://blog.integrisofthq.com" className={classes.gridItemLink}>Releases</a>
							</Grid>
						</Grid>
						<Grid item sm container spacing={1} direction="column" className={classes.gridColumn} >
							<Grid item className={classes.footerItem}>
								<Typography variant='subtitle1' className={classes.gridColumnTitle}>Learn More</Typography>
							</Grid>
							<Grid item className={classes.footerItem}>
								<Link href="/legal/terms" className={classes.gridItemLink}>Terms of Service</Link>
							</Grid>
							<Grid item className={classes.footerItem}>
								<Link href="/legal/privacy" className={classes.gridItemLink}>Cookie and Privacy Policy</Link>
							</Grid>
						</Grid>
						<Grid item sm container spacing={1} direction="column" className={classes.gridColumn} >
							<Grid item>
								<Typography variant='subtitle1' className={classes.gridColumnTitle}>Contact Us</Typography>
							</Grid>
							<Grid item className={classes.footerItem}>
								<EmailIcon color='primary' className={classes.textIcon} fontSize='small' />
								<a rel="noopener noreferrer" target="_blank" href="mailto:support@rubberduckstudying.com?subject=Contact%20Us%20-%20RubberDuckStudying&amp;body=Hello!" className={classes.gridItemLink}>support@rubberduckstudying.com</a>
							</Grid>
							<Grid item className={classes.footerItem}>
								<img className={classes.footerIcon} alt='slack' src="/static/images/Slack_Mark.svg" />
								<a rel="noopener noreferrer" target="_blank" href="https://rubberduckstudying.slack.com" className={classes.gridItemLink}>Join us on Slack!</a>
							</Grid>
						</Grid>
					</Grid>
				</Grid>

				{/* <Typography variant='subtitle1'>Terms of Service</Typography> */}
			</ Container >
			{/* <Divider variant='fullWidth' /> */}
			<Container maxWidth='md' className={classes.bottomContainer} >
				<Grid container justify='center' spacing={0} >
					{/* <Grid item xs container justify='center' className={classes.bottomContainerItem}>
						<Brand />
					</Grid> */}
					<Grid item xs container justify='center' className={classes.bottomContainerItem}>
						<Typography variant='caption' noWrap>Copyright © 2019 Integrisoft LLC</Typography>
					</Grid>
				</Grid>
			</ Container >
		</div >
	)
}
