import ReactDOM from 'react-dom';
import { Quill } from "react-quill";
// var Font = Quill.import('formats/font');
// Font.whitelist = ['Open Sans', 'Roboto'];
// Quill.register(Font, true);


class TextEditor extends React.Component {
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.modules = {
			syntax: {
				highlight: text => hljs.highlightAuto(text).value
			},
			toolbar: [
				// [{ 'font': [] }],
				[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
				[{ 'header': [1, 2, 3, 4, 5, 6, false] }],
				[{ 'header': 1 }, { 'header': 2 }],               // custom button values
				['bold', 'italic', 'underline', 'strike'],        // toggled buttons
				['blockquote', 'code-block'],

				[{ 'list': 'ordered' }, { 'list': 'bullet' }],
				[{ 'script': 'super' }, { 'script': 'sub' }],      // superscript/subscript
				[{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
				// [{ 'direction': 'rtl' }],                         // text direction


				[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
				[{ align: '' }, { align: 'center' }, { align: 'right' }, { align: 'justify' }],
				// ['link', 'image'],
				['link'],

				// ['clean']                                         // remove formatting button
			],
			clipboard: {
				matchVisual: false
			},
			keyboard: {
				bindings: {
					alignCenter: {
						key: 'E',
						shortKey: true,
						shiftKey: true,
						handler: function (range, context) {
							this.quill.format('align', 'center')
							return false
						},
					},
					alignLeft: {
						key: 'L',
						shortKey: true,
						shiftKey: true,
						handler: function (range, context) {
							this.quill.format('align', '')
							return false
						},
					},
					alignRight: {
						key: 'R',
						shortKey: true,
						shiftKey: true,
						handler: function (range, context) {
							this.quill.format('align', 'right')
							return false
						},
					},
					justify: {
						key: 'J',
						shortKey: true,
						shiftKey: true,
						handler: function (range, context) {
							this.quill.format('align', 'justify')
							return false
						},
					},
					superscript: {
						// ^
						key: 54,
						shortKey: true,
						shiftKey: true,
						handler: function (range, context) {
							if (context.format.script == 'super') {
								this.quill.format('script', '')
							} else {
								this.quill.format('script', 'super')
							}
							return false
						},
					},
					subscript: {
						// _
						key: 173,
						shortKey: true,
						shiftKey: true,
						handler: function (range, context) {
							if (context.format.script == 'sub') {
								this.quill.format('script', '')
							} else {
								this.quill.format('script', 'sub')
							}
							return false
						},
					},
				}
			}
		};
	}

	handleChange(content, delta, source, editor) {
		this.props.onChange(content)
	}

	componentDidMount() {
		let quill = new Quill(`#${this.props.id}`, {
			theme: this.props.theme,
			placeholder: this.props.placeholder,
			preserveWhitespace: true,
			modules: this.modules,
		})
		quill.root.innerHTML = this.props.value
		quill.spellcheck = true;
		quill.on('text-change', (delta, oldDelta, source) => {
			// if (source == 'api') {
			// 	// console.log("An API call triggered this change.");
			// } else 
			if (source == 'user') {
				this.props.onChange(quill.root.innerHTML)
			}
			if (this.props.maxLength) {
				if (quill.getLength() > this.props.maxLength) {
					quill.deleteText(this.props.maxLength, quill.getLength());
					document.getElementById(`${this.props.id}-container`).style.border = '1px solid #ff0033'
				} else {
					document.getElementById(`${this.props.id}-container`).style.border = 'none'
				}
			}
		});

		let showTooltip = (el) => {
			// Get the first class name of the element.
			// TODO: Could potentially be issues if the class name we need is in the middle of the list, but this can be a future improvement
			let tool = el.className.replace('ql-', '').split(' ');
			if (tool.length > 0) {
				tool = tool[0]
			}
			let value = el.value;
			if (tooltips[tool]) {
				var element = ReactDOM.findDOMNode(el);
				if (typeof tooltips[tool] == "object") {
					if (tooltips[tool][value]) {
						element.setAttribute("title", tooltips[tool][value]);
					}
				} else {
					element.setAttribute("title", tooltips[tool]);
				}
			}
		};

		// Setup the hover text for each toolbar button
		let tooltips = {
			'bold': 'Bold (Ctrl + B)',
			'italic': 'Italic (Ctrl + I)',
			'underline': 'Underline (Ctrl + U)',
			'strike': 'Strikethrough',
			'header': {
				1: 'Header 1',
				2: 'Header 2',
			},
			'blockquote': 'Block Quote',
			'code-block': 'Code Block',
			'list': {
				'ordered': 'Toggle Numbered List',
				'bullet': 'Toggle Bulleted List'
			},
			'script': {
				'super': 'Superscript (Ctrl + Shift + ^)',
				'sub': 'Subscript (Ctrl + Shift + _)',
			},
			'indent': {
				'-1': 'Decrease Indent',
				'+1': 'Increase Indent',
			},
			'color': 'Font Color',
			'background': 'Highlight Color',
			'align': {
				'': 'Align Left (Ctrl + Shift + L)',
				'center': 'Align Center (Ctrl + Shift + E)',
				'right': 'Align Right (Ctrl + Shift + R)',
				'justify': 'Justified (Ctrl + Shift + J)',
			},
			'clean': 'Clear Formatting',
			'image': 'Embed Image',
			'link': 'Embed Link',
		};

		let toolbarElement = document.querySelector('.ql-toolbar');
		if (toolbarElement) {
			let matches = toolbarElement.querySelectorAll('button');
			for (let el of matches) {
				showTooltip(el);
			}
			let spans = toolbarElement.getElementsByClassName('ql-picker')
			for (let el of spans) {
				showTooltip(el);
			}
		}
	}

	render() {
		return (
			<div style={{
				width: '100%',
				marginTop: '30px',
				marginBottom: '30px',
			}}
				id={`${this.props.id}-container`}
			>
				<div id={this.props.id} style={{ height: this.props.height ? this.props.height : '350px', fontSize: '14px' }}>

				</div>
			</div >
		)
	}
}
export default TextEditor;
