import { Paper, TextField, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/styles';
import { useState } from "react";
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '50px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	form: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		// alignContent: 'center',
		width: '80%',
	},
	textField: {
		maxWidth: '100%',
	},
	button: {
		marginTop: '20px',
		marginBottom: '30px',
	},
	title: {
		marginBottom: '10px',
	},
	subTitle: {
		opacity: '0.7',
	},
}));

function ForgotPasswordForm({ forgotPassword }) {

	const classes = useStyles();
	const [email, setEmail] = useState("");

	function handleSubmit(e) {
		e.preventDefault()
		forgotPassword(email)
	}

	return (
		<Paper className={classes.root}>
			<form className={classes.form} onSubmit={handleSubmit}>
				<Typography variant='h5' className={classes.title}>Forgot Password?</Typography>
				<Typography variant='subtitle1' className={classes.subTitle}>Enter your account email address and we will send you a link to reset your password.</Typography>
				<TextField
					autoFocus
					required
					id="email"
					label="Email"
					// placeholder="User"
					className={classes.textField}
					value={email}
					onChange={e => setEmail(e.target.value)}
					margin="normal"
				/>
				<Button variant="contained" color='primary' type='submit' className={classes.button}>Request Password Reset</Button>
			</form>
		</Paper>
	)

}

const mapStateToProps = state => ({

})

const mapDispatchToProps = ({ user: { forgotPassword } }) => ({
	forgotPassword: (email) => forgotPassword(email),
})

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordForm);
