import { Avatar, Container, CssBaseline, Divider, Typography } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import PopupState, { bindMenu, bindTrigger } from 'material-ui-popup-state';
import Router from 'next/router';
import React from 'react';
import { connect } from 'react-redux';
import Brand from './Brand';
import Link from './Link';
import NavDrawer from './NavDrawer';
import PageSpinner from './PageSpinner';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
	},
	brandContainer: {
		// marginRight: '15px',
	},
	grow: {
		flexGrow: 1,
	},
	button: {
		paddingTop: '5px',
		// padding: '9px 5px 5px 5px',
		marginLeft: '10px',
		paddingBottom: '5px',
		margin: theme.spacing(0),
		textTransform: 'none',
		"&:hover": {
			textDecoration: 'underline',
		}
	},
	navBar: {
		zIndex: 1401,
		boxShadow: 'none',
		height: '64px',
		marginBottom: '0px',
		display: 'flex',
		// backgroundColor: theme.palette.primary.dark,
		backgroundColor: '#fff',
		// flexWrap: 'wrap',
		// flexDirection: 'row',
		alignContent: 'center',
		// alignItems: 'center',
	},
	transparent: {
		backgroundColor: 'transparent',
	},
	avatar: {
		margin: '0 10px',
		backgroundColor: theme.palette.primary.main,
		"&:hover": {
			cursor: 'pointer',
		}
	},
	avatarMenu: {
		zIndex: '1402!important',
		width: '300px',
	},
	avatarMenuItem: {
		width: '300px',
	},
	avatarMenuHeader: {
		padding: '8px 16px 16px 16px',
	},
	avatarMenuUserName: {
		fontWeight: 500,
		marginBottom: '4px',
	},
	navLink: {
		// color: 'white',
	},
	toolBar: {
		height: '100%',
		padding: '0',
		[theme.breakpoints.down('sm')]: {
			padding: '5px',
		},
	},
	toolbarContainer: {
		height: '100%',
		[theme.breakpoints.down('sm')]: {
			padding: '0 0px',
		},
	},
}));

function RightAuthenticatedItems(props) {
	const classes = useStyles();
	return (
		<PopupState variant="popover" popupId="avatarMenu">
			{popupState => (
				<React.Fragment>
					<Link href='/dashboard'><Button className={classes.button}>Dashboard</Button></Link>
					<Link href='/courses'><Button className={classes.button}>Browse Courses</Button></Link>
					<Avatar className={classes.avatar} {...bindTrigger(popupState)}>
						{props.currentUser.name.length > 0 ? props.currentUser.name[0] : ''}
					</Avatar>
					<Menu
						className={classes.avatarMenu}
						{...bindMenu(popupState)}
						anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
						transformOrigin={{ vertical: "top", horizontal: "center" }}
						getContentAnchorEl={null}
					>
						<div className={classes.avatarMenuHeader}>
							<Typography className={classes.avatarMenuUserName} variant='body1'>{props.currentUser.name}</Typography>
							<Typography className={classes.avatarMenuCaption} variant='body2'>@{props.currentUser.username}</Typography>
						</div>
						<Divider />
						<MenuItem className={classes.avatarMenuItem} onClick={() => {
							Router.push('/account/profile')
							popupState.close
						}}>My Account</MenuItem>
						<Divider />
						<MenuItem className={classes.avatarMenuItem} onClick={() => {
							props.logout()
							popupState.close
						}}>Logout</MenuItem>
					</Menu>
				</React.Fragment>
			)}
		</PopupState>
	)
}

function RightUnauthenticatedItems(props) {
	const classes = useStyles();
	return (
		<>
			<Link href="/login" >
				<Button color="default" className={classes.button}>Log In</Button>
			</Link>
			<Link href="/signup" >
				<Button color="primary" className={classes.button}>Sign Up</Button>
			</Link>
		</>
	)
}

function LeftMenuItems(props) {
	const classes = useStyles();
	return (
		<>
			{/* <Link href='/resources'>
				<Button color="default" className={classes.button}>Resources</Button>
			</Link>
			<Link href='/support'>
				<Button color="default" className={classes.button}>Contact Us</Button>
			</Link> */}
		</>
	)
}

function RightMenuItems(props) {
	return (
		props.loggedIn ? <RightAuthenticatedItems {...props} /> : <RightUnauthenticatedItems />
	)
}

const mapStateToProps = state => ({
	loggedIn: state.user.loggedIn,
	profileLoaded: state.user.profileLoaded,
	currentUser: state.user.currentUser,
})

const mapDispatchToProps = ({ user: { logout } }) => ({
	logout: () => logout(),
})

export default connect(mapStateToProps, mapDispatchToProps)((props) => {
	const classes = useStyles();
	return (
		<>
			<CssBaseline />
			<AppBar position="static" className={`${classes.navBar} ${props.transparent ? "" : ""}`} >
				<Container className={classes.toolbarContainer} maxWidth='xl'>
					<Toolbar className={classes.toolBar}>
						<span className={classes.brandContainer}>
							<Brand dark />
						</span>
						<Hidden smDown>
							{props.profileLoaded ? <LeftMenuItems {...props} /> : ""}
							<div className={classes.grow} ></div>
							{props.profileLoaded ? <RightMenuItems {...props} /> : <PageSpinner />}
						</Hidden>
						<Hidden mdUp>
							<div className={classes.grow} ></div>
							<NavDrawer />
						</Hidden>
					</Toolbar >
				</Container>
			</ AppBar >
		</>
	)
})
