import { Avatar, Button, Card, Divider, Grid, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { formatUtcDate } from '../shared/dateformat';
import DeleteDialog from './DeleteDialog';

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: '20px',
		flexGrow: 1,
		marginBottom: '100px',
	},
	grow: {
		flexGrow: 1,
	},
	profileOverviewContainer: {
		display: 'flex',
		flexDirection: 'column',
		flexWrap: 'wrap',
		alignItems: 'center',
		alignContent: 'center',

	},
	profileOverviewGridContainer: {
		marginTop: '20px',
		padding: '10px 40px 40px 40px',
		alignItems: 'center',
		alignContent: 'center',
	},
	profileOverview: {
		marginTop: '20px',
		padding: '20px',
		[theme.breakpoints.down('xs')]: {
			textAlign: 'center',
		},
	},
	profilePageTitle: {
		marginTop: '20px',
		padding: '20px',
	},
	profileSectionTitle: {
		marginTop: '20px',
		marginBottom: '20px',
		// padding: '20px',
	},
	profileSection: {
		// marginTop: '20px',
		marginBottom: '20px',
		padding: '20px',
		[theme.breakpoints.down('sm')]: {
			textAlign: 'center',
		},
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
		flexWrap: 'wrap',
		alignItems: 'center',
		alignContent: 'center',
		textAlign: 'center',
	},
	profileAvatar: {
		height: '100px',
		width: '100px',
		fontSize: '36px',
		[theme.breakpoints.up('sm')]: {
			marginRight: '25px',
		},
		[theme.breakpoints.down('xs')]: {
			marginBottom: '20px',
		},
		backgroundColor: theme.palette.primary.main,
	},
	profileOverviewText: {
		// alignItems: 'center',
		// textAlign: 'center',
	},
	profileOverviewSubtitle: {
		opacity: '0.7',
	},
	form: {
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
		flexWrap: 'wrap',
		alignItems: 'center',
		[theme.breakpoints.down('xs')]: {
			alignContent: 'center',
			textAlign: 'center',
		},
		[theme.breakpoints.up('sm')]: {
			alignContent: 'flex-start',
		}
	},
	button: {
		margin: '30px 0 10px 0',
		alignContent: 'flex-end',
		width: '250px',

	},
	dangerButton: {
		color: theme.status.danger,
		borderColor: theme.status.danger,
		"&:hover": {
			color: 'white',
			backgroundColor: theme.status.danger,
		}
	},
	textField: {
		width: '100%',
		marginBottom: '20px',
		// maxWidth: 800,
	},
	success: {
		color: theme.status.success,
	},
	verifyEmailPrompt: {
		color: theme.status.danger,
		fontSize: '14px',
		textTransform: 'none',
		padding: 'none',
		fontWeight: 500,
	},
}))

function ProfileForm({ currentUser, updateCurrentUser, deleteCurrentUser, changeEmail, changePassword, resendVerificationEmail }) {
	if (currentUser == null) {
		return ""
	}

	const [email, setEmail] = useState(currentUser.email);
	const [changeEmailConfirmPassword, setChangeEmailConfirmPassword] = useState("");

	const [username, setUsername] = useState(currentUser.username);

	const [name, setName] = useState(currentUser.name);
	const [bio, setBio] = useState(currentUser.bio);

	const [newPassword, setNewPassword] = useState("");
	const [newPasswordError, setNewPasswordError] = useState(false);
	const [newPasswordHelperText, setNewPasswordHelperText] = useState("");
	const [newPasswordCurrentPassword, setNewPasswordCurrentPassword] = useState("");

	function handleSubmitUpdateUser(e) {
		e.preventDefault()
		let updateName = ""
		let updateBio = ""
		let updateUsername = ""
		if (name != currentUser.name) {
			updateName = name
		}

		if (username != currentUser.username) {
			updateUsername = username
		}

		if (bio != currentUser.bio) {
			updateBio = bio
		}

		if (name != currentUser.name || bio != currentUser.bio || username != currentUser.username) {
			updateCurrentUser({ name: updateName, bio: updateBio, username: updateUsername })
		}
	}

	async function handleSubmitChangeEmail(e) {
		e.preventDefault()
		if (email != currentUser.email) {
			await changeEmail({ newEmail: email, currentPassword: changeEmailConfirmPassword })
		}
	}

	async function handleSubmitChangePassword(e) {
		e.preventDefault()
		await changePassword({ newPassword: newPassword, currentPassword: newPasswordCurrentPassword })
		setNewPasswordError(false)
		setNewPasswordHelperText('')
		setNewPasswordCurrentPassword('')
		setNewPassword('')
	}

	function onNewPasswordChange(e) {
		setNewPassword(e.target.value)
		if (event.target.value.length == 0 || event.target.value.length >= 8) {
			setNewPasswordError(false)
			setNewPasswordHelperText('')
		} else {
			setNewPasswordError(true)
			setNewPasswordHelperText('Password must be a minimum of 8 characters')
		}
	}

	function VerifyEmailPrompt(props) {
		return <Typography variant='subtitle1'>
			<Button className={classes.verifyEmailPrompt} onClick={() => { resendVerificationEmail({ userID: currentUser.id }) }}>Verify Email</Button>
		</Typography>
	}

	const classes = useStyles();
	return (
		<div className={classes.root}>
			<Typography variant='h4' className={classes.profilePageTitle}>Account</Typography>
			<Divider variant='middle' />
			<section className={classes.profileOverview}>
				<Card className={classes.profileOverviewContainer}>
					<Grid container spacing={0} className={classes.profileOverviewGridContainer}>
						<Grid item xs={12}>
							<Grid container justify="center" spacing={0}>
								<Grid item>
									<Avatar className={classes.profileAvatar}>{currentUser.name[0]}</Avatar>
								</Grid>
								<Grid item className={classes.profileOverviewText}>
									<Typography variant='h6'>{currentUser.name}</Typography>
									<Typography variant='subtitle2' className={classes.profileOverviewSubtitle}>Email: {currentUser.email}
										{!currentUser.emailVerified ?
											<VerifyEmailPrompt /> : ""
										}
									</Typography>
									<Typography variant='subtitle2' className={classes.profileOverviewSubtitle}>Username: {currentUser.username}</Typography>
									<Typography variant='subtitle2' className={classes.profileOverviewSubtitle}>Member since {formatUtcDate(currentUser.createdAt)}</Typography>
									{/* <Typography variant='subtitle2' className={classes.profileOverviewSubtitle}>Last updated {formatUtcDate(currentUser.updatedAt)}</Typography> */}
								</Grid>
							</Grid>
						</Grid>
					</Grid>
				</Card>
			</section>
			<Divider variant='middle' />
			<section className={classes.profileSection}>
				<Typography variant='h5' className={classes.profileSectionTitle}>User Information</Typography>
				<form className={classes.form} onSubmit={handleSubmitUpdateUser}>
					<TextField
						required
						id="name"
						label="Name"
						className={classes.textField}
						value={name}
						onChange={e => setName(e.target.value)}
						margin="dense"
						inputProps={{
							maxLength: 100,
						}}
					/>
					<div className={classes.grow}></div>
					<TextField
						required
						id="username"
						label="Username"
						className={classes.textField}
						value={username}
						onChange={e => setUsername(e.target.value)}
						margin="dense"
					/>
					<TextField
						id="bio"
						multiline
						rows="4"
						label="Bio"
						variant='outlined'
						inputProps={{
							maxLength: 400,
						}}
						className={classes.textField}
						value={bio}
						onChange={e => setBio(e.target.value)}
						margin="dense"
					/>
					<div className={classes.grow}></div>
					<Button variant="outlined" color='default' type='submit' color='inherit' className={classes.button}>Save User Information</Button>
				</form>
			</section>
			<Divider variant='middle' />
			<section className={classes.profileSection}>
				<Typography variant='h5' className={classes.profileSectionTitle}>Change Email</Typography>
				<form className={classes.form} onSubmit={handleSubmitChangeEmail}>
					<TextField
						required
						id="email"
						label="Email"
						className={classes.textField}
						value={email}
						onChange={e => setEmail(e.target.value)}
						margin="dense"
					/>
					<TextField
						required
						id="changeEmailConfirmPassword"
						label="Current Password"
						type='password'
						className={classes.textField}
						value={changeEmailConfirmPassword}
						onChange={e => setChangeEmailConfirmPassword(e.target.value)}
						margin="dense"
						inputProps={{
							maxLength: 400,
						}}
					/>
					<div className={classes.grow}></div>
					<Button variant="outlined" color='default' type='submit' color='inherit' className={classes.button}>Change Email</Button>
				</form>
			</section>
			<Divider variant='middle' />
			<section className={classes.profileSection}>
				<Typography variant='h5' className={classes.profileSectionTitle}>Change Password</Typography>
				<form className={classes.form} onSubmit={handleSubmitChangePassword}>
					<TextField
						required
						id="newPassword"
						label="New Password"
						error={newPasswordError}
						helperText={newPasswordHelperText}
						type='password'
						className={classes.textField}
						value={newPassword}
						onChange={onNewPasswordChange}
						margin="dense"
					/>
					<TextField
						required
						id="changePasswordCurrentPassword"
						label="Current Password"
						type='password'
						className={classes.textField}
						value={newPasswordCurrentPassword}
						onChange={e => setNewPasswordCurrentPassword(e.target.value)}
						margin="dense"
					/>
					<div className={classes.grow}></div>
					<Button variant="outlined" color='default' type='submit' color='inherit' className={classes.button}>Change Password</Button>
				</form>
			</section>
			<Divider variant='middle' />
			<section className={classes.profileSection}>
				<Typography variant='h5' className={classes.profileSectionTitle}>Deactivate Account</Typography>
				<DeleteDialog
					buttonText="Delete my account"
					title="Are you sure you want to deactivate your account?"
					content="This will delete your account but will not delete any courses or tutorials that you created."
					onDelete={deleteCurrentUser}
					ariaLabel="deactivate-account"
				/>
			</section>
		</div>
	);
}


const mapStateToProps = state => ({
	currentUser: state.user.currentUser,
})

const mapDispatchToProps = ({ user: { login, updateCurrentUser, deleteCurrentUser, changeEmail, changePassword, resendVerificationEmail } }) => ({
	login: (userID, password) => login(userID, password),
	updateCurrentUser: (name) => updateCurrentUser(name),
	deleteCurrentUser: (name) => deleteCurrentUser(name),
	changeEmail: (newEmail, currentPassword) => changeEmail(newEmail, currentPassword),
	changePassword: (newPassword, currentPassword) => changePassword(newPassword, currentPassword),
	resendVerificationEmail: (userID) => resendVerificationEmail(userID),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileForm);