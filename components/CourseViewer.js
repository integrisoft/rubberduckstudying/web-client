import { Button, Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import moment from 'moment';
import pluralize from 'pluralize';
import { connect } from 'react-redux';
import Link from './Link';
import Tag from './Tag';
import UnitGoalViewer from './UnitGoalViewer';
const uuidv4 = require('uuid/v4')

const useStyles = makeStyles(theme => ({
	root: {
		// textAlign: 'center',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		width: '100%',
		paddingBottom: '50px',
	},
	header: {
		// padding: '30px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		// alignContent: 'center',
		// alignItems: 'center',
		width: '100%',
		background: 'linear-gradient(#29303B,#29303B,#29303B);',
		height: '500px',
		// backgroundColor: theme.palette.background.black,
		// background: 'url(https://raw.github.com/mmoustafa/Chalkboard/master/img/bg.png)',
		// marginBottom: '40px',
		textAlign: 'center',
		justifyContent: 'center',
	},
	overlay: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%',
		position: 'relative',
		zIndex: 1,
	},
	headerContainer: {
		padding: '50px 0 0px 0',
		width: '100%',
		display: 'flex',
		flexWrap: 'wrap',
		// [theme.breakpoints.down('md')]: {
		flexDirection: 'column',
		// },
		alignContent: 'center',
		alignItems: 'center',
		justifyContent: 'space-around',
	},
	headerTextContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		// alignContent: 'center',
		// alignItems: 'center',
		// textAlign: 'center',
		// justifyContent: 'center',
	},
	tagContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
		justifyContent: 'center',
		marginBottom: '20px',
	},
	headerText: {
		color: '#fff',
		// fontSize: '18px',
		marginBottom: '10px',
	},
	headerTitle: {
		color: '#fff',
		// marginBottom: '30px',
		// fontSize: '46px',
		// [theme.breakpoints.down('sm')]: {
		// 	fontSize: '32px',
		// },
		// alignSelf: 'center',
		// textAlign: 'center',
	},
	photoContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
	},
	courseActionContainer: {
		marginTop: '10px',
	},
	courseContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		// alignContent: 'center',
		// alignItems: 'center',
		width: '100%',
	},
	description: {
		width: '100%',
		fontSize: '15px',
		wordBreak: 'break-all',
		fontSize: '16px',
	},
	title: {
		marginTop: '20px',
		marginBottom: '20px',
	},
	courseAction: {
		margin: '0 5px 10px 5px',
	},
	dangerCourseAction: {
		margin: '0 5px 10px 5px',
		color: 'white',
		backgroundColor: theme.palette.danger.main,
		"&:hover": {
			color: 'white',
			backgroundColor: theme.palette.danger.dark,
		}
	},
	courseContent: {
		backgroundColor: '#f7f7f7',
		width: '100%',
	},
}));

function CourseViewer({ course, joinCourse, leaveCourse, selectedPhoto, setSelectedPhoto }) {
	const classes = useStyles()

	setSelectedPhoto(course.unsplashPhoto)
	const numUsers = course.users.length
	return (
		<div className={classes.root}>
			<div className={classes.header} style={{ background: selectedPhoto ? `url(${selectedPhoto.images.full + '&w=2048'})` : "" }}>
				<div className={classes.overlay}>
					<Container maxWidth='sm' className={classes.headerContainer}>
						{/* <Container maxWidth="sm" className={classes.headerTextContainer}> */}
						<Container maxWidth='sm' className={classes.headerTextContainer}>
							<Typography className={classes.headerTitle} variant='h1'>{course.name}</Typography>
							<Typography component='div' className={classes.headerText} variant='subtitle1'>{numUsers} {pluralize("student", numUsers)} enrolled</Typography>
							<Typography component='div' className={classes.headerText} gutterBottom variant='subtitle1'>Last Updated: {moment(course.updatedAt).format('MMMM Do YYYY, h:mm a')}</Typography>
							<Container maxWidth='md' className={classes.tagContainer}>
								{course.tags.map(tag => {
									return (
										<Tag key={`${tag}-${uuidv4()}`} name={tag} />
									)
								})}
							</Container>
							<div className={classes.photoContainer}>
								{/* <CourseImageViewer captionColor='light' editMode={false} /> */}
								<div className={classes.courseActionContainer}>
									{!course.courseRole.isMember ? <Button className={classes.courseAction} color='primary' variant='contained' onClick={() => {
										joinCourse(course.id)
									}}>Join Course</Button> : ""

									}
									{course.courseRole.manageCourse ?
										<Link href={`/course/edit/${course.slug}`}><Button className={classes.courseAction} variant='contained'>Update Course</Button></Link> : ""
									}
									{course.courseRole.isMember ? <Button className={classes.dangerCourseAction} variant='contained' onClick={async () => {
										await leaveCourse(course.id)
									}}>Leave Course</Button> : ""
									}
								</div>
							</div>
						</Container>
						<div>
						</div>
					</Container>
				</div>
			</div>
			<main className={classes.courseContent}>
				<Container className={classes.courseContainer} maxWidth="md">
					{/* <Typography className={classes.title} variant='h5'>Requirements</Typography> */}
					<Typography className={classes.title} variant='h2'>Description</Typography>
					<div className={classes.descriptionContainer}>
						<div className={classes.description} dangerouslySetInnerHTML={{ __html: course.description }} />
					</div>
					<Typography className={classes.title} variant='h2'>Content</Typography>
					<UnitGoalViewer course={course} />
				</Container>
			</main>
		</div >
	)
}

const mapStateToProps = state => ({
	selectedPhoto: state.unsplash.selectedPhoto,
})

const mapDispatchToProps = ({ course: { joinCourse, leaveCourse }, unsplash: { setSelectedPhoto } }) => ({
	joinCourse: (id) => joinCourse({ id }),
	leaveCourse: (id) => leaveCourse({ id }),
	setSelectedPhoto: (selectedPhoto) => setSelectedPhoto(selectedPhoto),
})

export default connect(mapStateToProps, mapDispatchToProps)(CourseViewer)