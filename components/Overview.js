import { Container, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PeopleIcon from '@material-ui/icons/People';
import SchoolIcon from '@material-ui/icons/School';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import React from 'react';

const useStyles = makeStyles(theme => ({
	root: {
		padding: '50px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		// backgroundColor: theme.palette.primary.dark,
		backgroundColor: '#f8f8f9',
		// backgroundColor: '#abe8ff;',
		// background: 'linear-gradient(0deg, rgba(6,117,154,1) 0%, rgba(6,117,154,1) 2%, rgba(3,72,95,1) 50%)',
	},
	overviewContainer: {
		// padding: '40px',
		// textAlign: 'center',
		// minHeight: '150px',
		padding: '20px',
		display: 'flex',
		// flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
		margin: '0',
	},
	titleText: {
		color: theme.palette.primary.dark,
		marginBottom: '20px',
	},
	overviewText: {
		color: theme.palette.text.primary,
	},
	overviewTextQuoteAttribute: {
		color: theme.palette.text.primary,
		marginBottom: '30px',
	},
	overviewTextQuote: {
		fontStyle: 'italic',
		marginBottom: '5px',
		color: theme.palette.text.primary,
		opacity: '0.8',
		fontWeight: 450,
	},
	overviewItems: {
		// flexDirection: 'row',
		// marginTop: '50px',
		flexGrow: 1,
	},
	overviewItem: {
		// flexDirection: 'row',
		// padding: '0 50px 0 50px',
		// marginRight: '20px',
		textAlign: 'center',
	},
	overviewItemTitle: {
		marginBottom: '20px',
		fontWeight: 450,
	},
	overviewItemIcon: {
		marginBottom: '20px',
		fontSize: '32px',
		// flexDirection: 'row',
		// padding: '0 50px 0 50px',
		// marginRight: '20px',
	},

}));

export default function Overview() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Container maxWidth='lg' className={classes.overviewContainer}>
				<Typography variant='h2' className={classes.titleText}>Why RubberDuckstudying?</Typography>
				<Typography variant='subtitle1' className={classes.overviewTextQuote}>Tell me and I forget, teach me and I may remember, involve me and I learn.</Typography>
				<Typography variant='subtitle1' className={classes.overviewTextQuoteAttribute}>- Benjamin Franklin</Typography>
				<Grid container item direction="row" justify="center" className={classes.overviewItems} spacing={6}>
					<Grid item sm className={classes.overviewItem}>
						<SchoolIcon className={classes.overviewItemIcon} color='secondary' />
						<Typography variant='h3' className={classes.overviewItemTitle}>Learn</Typography>
						<Typography variant='body1' className={classes.overviewText}>
							Whether you're a current student needing to study for an exam or a working adult,
							RubberDuckStudying can teach you how to use free online resources to achieve your goals.
						</Typography>
					</Grid>
					<Grid item sm className={classes.overviewItem}>
						<PeopleIcon className={classes.overviewItemIcon} color='secondary' />
						<Typography variant='h3' className={classes.overviewItemTitle}>Connect</Typography>
						<Typography variant='body1' className={classes.overviewText}>
							Join other students from across the world or bring your friends to create your own study groups.
						</Typography>
					</Grid>
					<Grid item sm className={classes.overviewItem}>
						<TrendingUpIcon className={classes.overviewItemIcon} color='secondary' />
						<Typography variant='h3' className={classes.overviewItemTitle}>Grow</Typography>
						<Typography variant='body1' className={classes.overviewText}>
							Improve your own understanding of a topic and share your knowledge with others to reinforce what you've learned.
							Studies have shown that students who spend time teaching what they've learned show an improvement in understanding
							and knowledge retention over students who simply study the material.
						</Typography>
						{/* </Grid> */}
					</Grid>
					{/* </Grid> */}
				</Grid>
			</Container>

		</div>
	);
}