import { Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import dynamic from "next/dynamic";
import React, { useState } from 'react';
import { connect } from 'react-redux';

const DynamicTextEditor = dynamic(() => import("./TextEditor"), { ssr: false }); <DynamicTextEditor />

const useStyles = makeStyles(theme => ({
	quillContainer: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		maxWidth: '100%',
		minWidth: '300px',
		// alignContent: 'center',
		// alignItems: 'center',
	},
	formActionContainer: {
		display: 'flex',
	},
	formAction: {
		marginLeft: '20px',
	},
	grow: {
		flexGrow: 1,
	},
}))

const GoalEditForm = ({ goal, onCancel, updateGoal }) => {
	const classes = useStyles()
	const [name, setName] = useState(goal.name)
	const [description, setDescription] = useState(goal.description)

	const onSubmit = async (e) => {
		e.preventDefault()
		await updateGoal({
			id: goal.id,
			name: name,
			description: description,
		})
		onCancel()
	}

	return (
		<form onSubmit={onSubmit}>
			<div className={classes.quillContainer}>
				<TextField
					autoFocus
					required
					inputProps={{
						maxLength: 30,
					}}
					label="Goal Name"
					className={classes.nameTextField}
					value={name}
					onChange={e => { setName(e.target.value) }}
					margin="normal"
				/>
				<DynamicTextEditor maxLength={4000} id="goalDescription" height="400px" theme='snow' value={description} placeholder="Description" onChange={(value) => {
					setDescription(value)
				}} />
			</div>
			<div className={classes.formActionContainer}>
				<div className={classes.grow}></div>
				<Button variant='contained' onClick={() => {
					onCancel()
				}} className={classes.formAction}>Cancel</Button>
				<Button variant='contained' color='primary' type='submit' className={classes.formAction}>Update Goal</Button>
			</div>
		</form>
	)
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = ({ goal: { updateGoal } }) => ({
	updateGoal: (id, name, description) => updateGoal(id, name, description),
})

export default connect(mapStateToProps, mapDispatchToProps)(GoalEditForm);
