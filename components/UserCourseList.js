import { Button, Container, makeStyles, Typography } from '@material-ui/core';
import gql from 'graphql-tag';
import { useState } from 'react';
import { useQuery } from 'react-apollo';
import { connect } from 'react-redux';
import { photoFields } from '../shared/models/schemaFields';
import CourseList from './CourseList';
import EmptyCourseList from './EmptyCourseList';
import ErrorHandler from './ErrorHandler';
import Link from './Link';
import Loading from './Loading';

const GET_USER_COURSES = gql`
  query user_profile($orderBy: String, $offset: Int, $limit: Int) {
    profile {
			id
      courses(orderBy: $orderBy, offset: $offset, limit: $limit) {
				id
				slug
				name
				description
				unsplashImageId
				unsplashPhoto {
					${photoFields.join(',')}
				}
				tags
				users {
					id
				}
				units {
					id
					slug
					goals {
						id
						slug
						tutorials {
							id
						}
					}
				}
			}
    }
  }
`;

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
		alignItems: 'center',
	},
	title: {
		marginTop: '20px',
		marginBottom: '20px',
	},
	courseListContainer: {
		margin: '30px 0',
	},
}));

function UserCourseList(props) {
	const classes = useStyles()
	const [numCoursesInLastFetch, setNumCoursesInLastFetch] = useState(-1)
	const { data, error, fetchMore, networkStatus } = useQuery(GET_USER_COURSES, {
		variables: {
			orderBy: "updated_at desc",
			offset: 0,
			limit: 20,
		},
		notifyOnNetworkStatusChange: true,
		fetchPolicy: 'network-only',
	});

	if (error) {
		return <ErrorHandler error={error} />
	}
	if (!data || !data.profile.courses) {
		return <Loading />;
	}
	if (data.profile.courses.length == 0) {
		return (
			<EmptyCourseList message="Looks like you aren't in any courses yet." create={true} browse={true} />
		)
	}
	return (
		<Container maxWidth='lg'>
			<Typography variant='h5' className={classes.title}>My Courses</Typography>
			<Link href='/course/create'>
				<Button color='primary' variant='contained'>New Course</Button>
			</Link>
			<div className={classes.courseListContainer}>
				<CourseList courses={data.profile.courses} numCoursesInLastFetch={numCoursesInLastFetch} fetchMore={fetchMore} updateQuery={(prev, fetchMoreResult) => {
					setNumCoursesInLastFetch(fetchMoreResult.profile.courses.length)
					const updatedResult = {
						profile: prev.profile
					};
					updatedResult.profile.courses = [...prev.profile.courses, ...fetchMoreResult.profile.courses]
					return updatedResult
					// return Object.assign({}, prev, {
					// 	courses: 
					// });
				}} networkStatus={networkStatus} perPage={20} />
			</div>
		</Container >
	)
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = ({ statusMessage: { setStatus } }) => ({
	setStatus: (variant, message) => setStatus({ variant: variant, message: message }),
})

export default connect(mapStateToProps, mapDispatchToProps)(UserCourseList)