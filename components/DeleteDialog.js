import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useState } from 'react';

const useStyles = makeStyles(theme => ({
	button: {
		margin: '20px 0 10px 0',
		alignContent: 'flex-end',
	},
	dangerButton: {
		color: theme.status.danger,
		borderColor: theme.status.danger,
		"&:hover": {
			color: 'white',
			backgroundColor: theme.status.danger,
		},
		fontSize: '14px',
	},
}))

export default function DeleteDialog(props) {
	const [deletePromptOpen, setDeletePromptOpen] = useState(false);
	const classes = useStyles()

	function handleOpenDeleteDialog() {
		setDeletePromptOpen(true)
	}

	function handleCloseDeleteDialog() {
		setDeletePromptOpen(false)
	}

	return (
		<div>
			<Button variant="outlined" color="primary" className={classes.dangerButton} onClick={handleOpenDeleteDialog}>
				{props.buttonText}
			</Button>
			<Dialog
				open={deletePromptOpen}
				onClose={handleCloseDeleteDialog}
				aria-labelledby={props.ariaLabel}
				aria-describedby={`${props.ariaLabel}-description`}
			>
				<DialogTitle>{props.title}</DialogTitle>
				<DialogContent>
					<DialogContentText id={`${props.ariaLabel}-description`}>
						{props.content}
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleCloseDeleteDialog} color="default" autoFocus>
						Cancel
				</Button>
					<Button onClick={() => {
						props.onDelete()
						handleCloseDeleteDialog()
					}
					} className={classes.dangerButton}>
						{props.buttonText}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}
