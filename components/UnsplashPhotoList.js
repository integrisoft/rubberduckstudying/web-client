import { makeStyles, Paper, Typography } from "@material-ui/core";
import { connect } from 'react-redux';
import SearchBar from './SearchBar';
import UnsplashPhotoGrid from './UnsplashPhotoGrid';

const useStyles = makeStyles(theme => ({
	root: {
		width: '370px',
		// minWidth: '200px',
		padding: '20px',
	},
	titleContainer: {
		// display: 'flex',
		// flexDirection: 'row',
		marginBottom: '20px',
		textAlign: 'center',
	},
	grow: {
		flexGrow: 1,
	},
	photoTitle: {
		color: '#666',
	},
	searchContainer: {
		marginBottom: '20px',
	},
	photoTitleLink: {
		color: '#666',
		textDecoration: 'none',
		"&:hover": {
			textDecoration: 'underline',
		}
	}
}));

const UnsplashPhotoList = (props) => {
	const classes = useStyles()
	return (
		<Paper className={classes.root}>
			<div className={classes.titleContainer}>
				<Typography className={classes.photoTitle} variant='h6'>Photos By <a className={classes.photoTitleLink} href="https://unsplash.com/?utm_source=rubberduckstudying&utm_medium=referral">Unsplash</a></Typography>
			</div>
			<div className={classes.searchContainer}>
				<SearchBar placeHolder='Search Images' setSearchQuery={props.setSearchQuery} />
			</div>
			<UnsplashPhotoGrid searchQuery={props.searchQuery} setSelectedPhotoInfo={props.setSelectedPhotoInfo} />
		</Paper>
	)
}

const mapStateToProps = state => ({
	searchQuery: state.unsplash.searchQuery,
})

const mapDispatchToProps = ({ unsplash: { setSearchQuery } }) => ({
	setSearchQuery: (searchQuery) => setSearchQuery(searchQuery),
})

export default connect(mapStateToProps, mapDispatchToProps)(UnsplashPhotoList)

