import { Breadcrumbs, Button, Container, IconButton, Modal } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import EditIcon from '@material-ui/icons/Edit';
import pluralize from 'pluralize';
import React from 'react';
import { connect } from 'react-redux';
import { formatUtcDate } from '../shared/dateformat';
import GoalEditForm from './GoalEditForm';
import Link from './Link';
import ResourceDrawer from './ResourceDrawer';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
	},
	// toolbar: theme.mixins.toolbar,
	content: {
		display: 'flex',
		flexGrow: 1,
		// padding: theme.spacing(3),
		padding: '48px 24px',

	},
	descriptionContainer: {
		marginBottom: '40px',
		display: 'flex',
		// alignItems: 'center',
		alignContent: 'center',
	},
	description: {
		width: '100%',
		fontSize: '15px',
		wordBreak: 'break-all',
	},
	goalNav: {
		marginBottom: '50px',
	},
	editButton: {
		marginLeft: '10px',
		"&:hover": {
			cursor: 'pointer',
		},
		verticalAlign: 'text-bottom',
	},
	paper: {
		position: 'absolute',
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: '30px 40px',
	},
	title: {
		marginTop: '25px',
		marginBottom: '20px',
		fontWeight: 500,
	},
	modalTitle: {
		// display: 'inline',
	},
	closeIcon: {
		// display: 'inline',
	},
	modalActionContainer: {
		display: 'flex',
	},
	grow: {
		flexGrow: 1,
		// display: 'inline-block',
	},
	tutorialContainer: {
		margin: '20px 0px',
	},
	tutorialTitleContainer: {
		display: 'flex',
		alignItems: 'flex-end',
		marginBottom: '5px',
	},
	tutorialTitle: {
		marginRight: '10px',
	},
	tutorialHeader: {
		display: 'flex',
		alignItems: 'flex-end',
		marginBottom: '15px',
	},
	starContainer: {
	},
	unStarredIcon: {
		// paddingTop: '5px',
		color: '#ddd',
		marginRight: '5px',
		fontSize: '34px',
		"&:hover": {
			color: '#ffd700',
			cursor: 'pointer',
		},
	},
	starredIcon: {
		color: '#ffd700',
		marginRight: '5px',
		fontSize: '34px',
		"&:hover": {
			color: '#ddd',
			cursor: 'pointer',
		},
	},
	headerItem: {
		display: 'flex',
		alignItems: 'flex-end',
		marginRight: '10px',
	},
	tutorialList: {
		marginBottom: '50px',
	},
	button: {
		marginBottom: '20px',
	},
}));

function getModalStyle() {
	const top = 50;
	const left = 50;

	return {
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
	};
}


function TutorialList(props) {

	function isStarred(tutorialStars) {
		return tutorialStars.some(star => { return star.id == props.currentUser.id });
	}

	const classes = useStyles()
	return (
		<>
			{props.goal.tutorials.map((tutorial) => {
				// Terribly inefficient but will work for now
				// Should be able to retrieve separate lists for "My Tutorials" and "Other Tutorials"
				if (props.includeAuthorID && tutorial.author.id != props.includeAuthorID) {
					return ""
				} else if (props.excludeAuthorID && tutorial.author.id == props.excludeAuthorID) {
					return ""
				}

				return (
					<React.Fragment key={tutorial.id}>
						<div className={classes.tutorialContainer}>
							<div className={classes.tutorialTitleContainer}>
								<Link href={`/course/${props.goal.parentUnit.parentCourse.slug}/${props.goal.parentUnit.slug}/${props.goal.slug}/tutorial/${tutorial.id}`}>
									<Typography className={classes.tutorialTitle} component='span' variant='h5'>{tutorial.title}</Typography>
								</Link>
								{/* <StarIcon onClick={() => {
									props.toggleStar(tutorial.id)
									// forceUpdate()
								}} className={isStarred(tutorial.stars) ? classes.starredIcon : classes.unStarredIcon} /> */}
							</div>
							<div className={classes.tutorialHeader}>
								<span className={classes.headerItem}>
									<Typography component='span' variant='body2'>{tutorial.stars.length} {pluralize('star', tutorial.stars.length)}</Typography>
								</span>
								<span className={classes.headerItem}>
									<Typography variant='caption'>Author: <Link href={`/user/${tutorial.author.username}`}><Typography variant='caption' color='secondary'>{tutorial.author.username}</Typography></Link></Typography>
								</span>
								<span className={classes.headerItem}>
									<Typography variant='caption'>Last Updated {formatUtcDate(tutorial.updatedAt)}</Typography>
								</span>
							</div>
						</div>
						<Divider />
					</React.Fragment>
				)
			})}
		</>
	)
}

function GoalViewer(props) {
	if (props.currentUser == null) {
		return ""
	}

	const classes = useStyles();
	const [modalStyle] = React.useState(getModalStyle);
	const [editGoalModalOpen, setEditGoalModalOpen] = React.useState(false);

	return (
		<div className={classes.root}>
			<CssBaseline />
			<main className={classes.content}>
				{/* <div className={classes.toolbar} /> */}
				<Container maxWidth="md">
					<Breadcrumbs aria-label="breadcrumb" className={classes.goalNav}>
						<Link color="inherit" href={`/course/${props.goal.parentUnit.parentCourse.slug}`}>
							{props.goal.parentUnit.parentCourse.name}
						</Link>
						<Typography color='textPrimary'>{props.goal.parentUnit.name}</Typography>
						<span>
							<Typography component='span' color='textPrimary'>{props.goal.name}</Typography>
							{props.goal.parentUnit.parentCourse.courseRole.manageCourse ?
								<EditIcon className={classes.editButton} fontSize='small' onClick={() => setEditGoalModalOpen(true)} /> :
								""
							}
						</span>
					</Breadcrumbs>
					<Typography className={classes.title} variant='h5'>Description</Typography>
					<div className={classes.descriptionContainer}>
						<div className={classes.description} dangerouslySetInnerHTML={{ __html: props.goal.description }} />
					</div>
					<section className={classes.tutorialList}>
						<Typography className={classes.title} variant='h5'>My Tutorials</Typography>
						<Link href={`/course/${props.goal.parentUnit.parentCourse.slug}/${props.goal.parentUnit.slug}/${props.goal.slug}/tutorial/create`}>
							<Button color='primary' size='medium' variant='contained' className={classes.button}>Create Tutorial</Button>
						</Link>
						<TutorialList edit={true} goal={props.goal} currentUser={props.currentUser} toggleStar={props.toggleStar} includeAuthorID={props.currentUser.id} />
					</section>
					<section className={classes.tutorialList}>
						<Typography className={classes.title} variant='h5'>Other Tutorials</Typography>
						<TutorialList goal={props.goal} currentUser={props.currentUser} toggleStar={props.toggleStar} excludeAuthorID={props.currentUser.id} />
					</section>
				</Container>
			</main>
			<ResourceDrawer />
			<Modal
				aria-labelledby="goal-edit-form"
				open={editGoalModalOpen}
				onClose={() => setEditGoalModalOpen(false)}
			>
				<div style={modalStyle} className={classes.paper}>
					<div className={classes.modalActionContainer}>
						<Typography variant='h6' id="goal-edit-form" >Edit Goal</Typography>
						<div className={classes.grow}></div>
						<IconButton className={classes.closeIcon} aria-label="Close Image Selection" onClick={() => setEditGoalModalOpen(false)}>
							<CloseIcon />
						</IconButton>
					</div>
					<GoalEditForm goal={props.goal} onCancel={() => setEditGoalModalOpen(false)} />
				</div>
			</Modal>
		</div>
	);
}

const mapStateToProps = state => ({
	currentUser: state.user.currentUser,
})

const mapDispatchToProps = ({ tutorial: { toggleStar } }) => ({
	toggleStar: (tutorialID) => toggleStar({ tutorialID })
})

export default connect(mapStateToProps, mapDispatchToProps)(GoalViewer);

