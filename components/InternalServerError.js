import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: '100px',
		flexGrow: 1,
		textAlign: 'center',
	},
}))

export default ({ error }) => {
	const classes = useStyles()
	return (
		<Container className={classes.root} maxWidth='md'>
			<img
				src={'/static/images/undraw_server_down.svg'}
				alt="ServerError"
				height={'256px'}
				style={{
					marginBottom: '50px',
				}} />
			<Typography variant='h6' gutterBottom>Something went wrong on our end. Try refreshing this page or contact us at <a
				rel="noopener noreferrer"
				target="_blank"
				href={
					`mailto:support@rubberduckstudying.com?
					subject=Internal%20Server%20Error%20-%20RubberDuckStudying&amp;
					body=${error && error.message ? `Hello, I received the following error message on rubberduckstudying.com: ${error.message}` : ""}`}
				className={classes.gridItemLink}>support@rubberduckstudying.com
			</a> if this problem persists.</Typography>
			<img
				src={'/static/images/horizontal-logo.svg'}
				alt="RubberDuckStudying"
				height={'32px'}
				style={{
					marginTop: '30px',
				}} />
		</Container >
	)
}