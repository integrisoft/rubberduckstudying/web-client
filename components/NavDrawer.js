import { IconButton, Link, Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import React from 'react';
import { connect } from 'react-redux';
const uuidv4 = require('uuid/v4');

const useStyles = makeStyles(theme => ({
	navDrawer: {
		// The Drawer element has a 1300 z-index for its root, but we need to make sure it stays above the app bar which has a
		// z-index of 1401
		zIndex: '1402!important',
	},
	list: {
		width: 250,
	},
	listItem: {
		// textAlign: 'center',
	},
	linkNormal: {
		color: theme.palette.text.primary,
		textDecoration: 'none',
		"&:hover": {
			textDecoration: 'none',
		},
		width: '100%',
		// textAlign: 'center',
	},
	button: {
		width: '80%',
		textTransform: 'none',
	},
	avatarMenuHeader: {
		padding: '8px 16px 16px 16px',
	},
	avatarMenuUserName: {
		fontWeight: 500,
		marginBottom: '4px',
	},
	primaryLink: {
		color: theme.palette.primary.main,
	},
}));

function TemporaryDrawer(props) {
	const classes = useStyles();
	const [state, setState] = React.useState({
		open: false,
	});

	const setOpen = (isOpen) => event => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}

		setState({ ...state, open: isOpen });
	};

	const AuthenticatedList = () => (
		<div
			className={classes.list}
			role="presentation"
			onClick={setOpen(false)}
			onKeyDown={setOpen(false)}
		>
			<List>
				<div className={classes.avatarMenuHeader}>
					<Typography className={classes.avatarMenuUserName} variant='body1'>{props.currentUser.name}</Typography>
					<Typography className={classes.avatarMenuCaption} variant='body2'>@{props.currentUser.username}</Typography>
				</div>
				<Divider />
				<ListItem className={classes.listItem} button key={uuidv4()}>
					<Link href='/dashboard' className={classes.linkNormal}><ListItemText primary='Dashboard' /></Link>
				</ListItem>
				<ListItem className={classes.listItem} button key={uuidv4()}>
					<Link href='/courses' className={classes.linkNormal}><ListItemText primary='Browse Courses' /></Link>
				</ListItem>
			</List>
			<Divider />
			<List>
				<ListItem className={classes.listItem} button key={uuidv4()}>
					<Link href='/account/profile' className={classes.linkNormal}><ListItemText primary='My Account' /></Link>
				</ListItem>
				<ListItem className={classes.listItem} button key={uuidv4()}>
					<ListItemText primary='Log Out' onClick={props.logout} />
				</ListItem>
			</List>
		</div>
	);

	const UnauthenticatedList = () => (
		<div
			className={classes.list}
			role="presentation"
			onClick={setOpen(false)}
			onKeyDown={setOpen(false)}
		>
			<List>
				<ListItem button key={uuidv4()}>
					<Link href='/about' className={classes.linkNormal}><ListItemText primary='About' /></Link>
				</ListItem>
				<ListItem button key={uuidv4()}>
					<a href='https://blog.integrisofthq.com' className={classes.linkNormal}><ListItemText primary='Blog' /></a>
				</ListItem>
			</List>
			<Divider />
			<List>
				<ListItem button key={uuidv4()}>
					{/* <ListItemIcon><InboxIcon /></ListItemIcon> */}
					<Link href='/login' className={classes.linkNormal}><ListItemText primary='Log in' /></Link>
				</ListItem>
				<ListItem button key={uuidv4()}>
					<Link href='/signup' className={classes.linkNormal}><ListItemText className={classes.primaryLink} primary='Sign Up' /></Link>
				</ListItem>
			</List>
		</div>
	);


	return (
		<div>
			<IconButton edge="start" aria-label="menu" onClick={setOpen(true)}>
				<MenuIcon />
			</IconButton>
			<Drawer
				anchor="right"
				open={state.open}
				className={classes.navDrawer}
				onClose={setOpen(false)}
				ModalProps={{
					keepMounted: true, // Better open performance on mobile.
				}}
			>
				{props.loggedIn ? <AuthenticatedList /> : <UnauthenticatedList />}
			</Drawer>
		</div>
	);
}

const mapStateToProps = state => ({
	loggedIn: state.user.loggedIn,
	profileLoaded: state.user.profileLoaded,
	currentUser: state.user.currentUser,
})

const mapDispatchToProps = ({ user: { logout } }) => ({
	logout: () => logout(),
})

export default connect(mapStateToProps, mapDispatchToProps)(TemporaryDrawer)