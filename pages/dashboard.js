import Container from '@material-ui/core/Container';
import React from 'react';
import App from '../components/App';
import NavBar from '../components/NavBar';
import TitleBanner from '../components/TitleBanner';
import UserCourseList from '../components/UserCourseList';
import withApollo from '../shared/withApollo';
import { withAuthSync } from '../shared/withAuth';

class Dashboard extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<App>
				<NavBar />
				<TitleBanner title='My Dashboard' />
				<Container maxWidth="lg">
					<UserCourseList />
				</Container>
			</App >
		)
	}
}

Dashboard.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default withApollo(withAuthSync(Dashboard));
