import { Container } from '@material-ui/core';
import React from 'react';
import App from '../../components/App';
import CourseBrowser from '../../components/CourseBrowser';
import NavBar from '../../components/NavBar';
import TitleBanner from '../../components/TitleBanner';
import withApollo from '../../shared/withApollo';
import { withAuthSync } from '../../shared/withAuth';

function BrowseCourses(props) {
	return (
		<App>
			<NavBar />
			<TitleBanner title='All Courses' />
			<Container>
				{/* <Typography variant='h1' gutterBottom style={{ marginBottom: '20px' }}>All Courses</Typography> */}
				<CourseBrowser initialSearchQuery={props.initialSearchQuery} />
			</Container>
		</App>
	)
}

BrowseCourses.getInitialProps = async function ({ req, query, reduxStore }) {
	console.log(query)
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {
		initialSearchQuery: query.query,
		title: 'Browse'
	}
}

export default withApollo(withAuthSync(BrowseCourses));
