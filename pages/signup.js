import { Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import App from '../components/App';
import Link from '../components/Link';
import NavBar from '../components/NavBar';
import SignUpForm from '../components/SignUpForm';
import withApollo from '../shared/withApollo';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '100px 20px 50px 20px',
		backgroundColor: theme.palette.primary.main,
		height: '100vh',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
	},
	loginMessage: {
		marginTop: '20px',
		color: '#eee',
	},
	loginLink: {
		color: 'white',
		textDecoration: 'underline',
	},
}));

function SignUp(props) {
	const classes = useStyles();
	return (
		<App >
			<NavBar />
			<div className={classes.root}>
				<Container maxWidth="sm">
					<SignUpForm />
					<Typography variant='subtitle1' className={classes.loginMessage}>Already have an account? <Link href='/login' className={classes.loginLink}>Log in</Link></Typography>
				</Container>
			</div>
		</App>
	)
}

SignUp.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default withApollo(SignUp)