import { Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import App from '../components/App';
import Link from '../components/Link';
import LoginForm from '../components/LoginForm';
import NavBar from '../components/NavBar';
import withApollo from '../shared/withApollo';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '100px 20px 50px 20px',
		backgroundColor: theme.palette.primary.main,
		height: '100vh',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
	},
	signupMessage: {
		marginTop: '20px',
		color: '#eee',
	},
	signupLink: {
		color: 'white',
		textDecoration: 'underline',
	},
}));

function Login(props) {
	const classes = useStyles();
	return (
		<App >
			<NavBar />
			<div className={classes.root}>
				<Container maxWidth="xs">
					<LoginForm redirectTo={props.query.to} />
					<Typography variant='subtitle1' className={classes.signupMessage}>Don't have an account? <Link href='/signup' className={classes.signupLink}>Create one now</Link></Typography>
				</Container>
			</div>
		</App>
	)
}

Login.getInitialProps = async function ({ req, query, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return { query }
}

export default withApollo(Login)