import { Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import React from 'react';
import App from '../components/App';
import NavBar from '../components/NavBar';

function About(props) {
	return (
		<App>
			<NavBar />
			<Container maxWidth="lg">
				<Typography variant='h1'>Resources</Typography>
			</Container>
		</App >
	)
}

About.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default About