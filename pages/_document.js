import { ServerStyleSheets } from '@material-ui/styles';
import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';
import { initialize, isEnabled } from 'unleash-client';
import { Maintenance } from '../components/Maintenance';
import theme from '../theme';

const env = process.env.NODE_ENV || "development";
const devMode = env == "development"
if (!devMode) {
	const instance = initialize({
		url: process.env.UNLEASH_URL,
		appName: 'rubberduckstudying',
		instanceId: process.env.UNLEASH_INSTANCE_ID,
	});
	// optional events
	instance.on('error', console.error);
	instance.on('warn', console.warn);
	instance.on('ready', console.log);

	// metrics hooks
	instance.on('registered', clientData => console.log('registered', clientData));
	instance.on('sent', payload => console.log('metrics bucket/payload sent', payload));
	instance.on('count', (name, enabled) => console.log(`isEnabled(${name}) returned ${enabled}`));
}

class MyDocument extends Document {
	render() {
		const maintenanceMode = devMode ? false : isEnabled('maintenance_mode')
		return (
			<html lang="en">
				<Head>
					<meta charSet="utf-8" />
					{/* Use minimum-scale=1 to enable GPU rasterization */}
					<meta
						name="viewport"
						content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
					/>
					{/* PWA primary color */}
					<meta name="theme-color" content={theme.palette.primary.main} />
					<link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great&display=swap" rel="stylesheet" />
					<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet" />
				</Head>
				<body>
					{maintenanceMode ? <Maintenance /> : <Main />}
					<NextScript />
				</body>
			</html>
		);
	}
}

MyDocument.getInitialProps = async ctx => {
	// Resolution order
	//
	// On the server:
	// 1. app.getInitialProps
	// 2. page.getInitialProps
	// 3. document.getInitialProps
	// 4. app.render
	// 5. page.render
	// 6. document.render
	//
	// On the server with error:
	// 1. document.getInitialProps
	// 2. app.render
	// 3. page.render
	// 4. document.render
	//
	// On the client
	// 1. app.getInitialProps
	// 2. page.getInitialProps
	// 3. app.render
	// 4. page.render

	// Render app and page and get the context of the page with collected side effects.
	const sheets = new ServerStyleSheets();
	const originalRenderPage = ctx.renderPage;

	ctx.renderPage = () =>
		originalRenderPage({
			enhanceApp: App => props => sheets.collect(<App {...props} />),
		});

	const initialProps = await Document.getInitialProps(ctx);

	return {
		...initialProps,
		// Styles fragment is rendered after the app and page rendering finish.
		styles: [
			<React.Fragment key="styles">
				{initialProps.styles}
				{sheets.getStyleElement()}
			</React.Fragment>,
		],
	};
}

export default MyDocument;