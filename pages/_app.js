import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import App from 'next/app';
import Head from 'next/head';
import React from 'react';
import { Provider } from 'react-redux';
import withRematch from '../shared/withRematch';
import theme from '../theme';

class MyApp extends App {
	static async getInitialProps({ Component, router, ctx }) {
		let pageProps = {}

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx)
		}
		return { pageProps }
	}

	componentDidMount() {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side');
		if (jssStyles) {
			jssStyles.parentNode.removeChild(jssStyles);
		}
	}
	withRematch
	render() {
		const { Component, pageProps, reduxStore, title } = this.props
		return (
			<>
				<Head>
					{pageProps.title ?
						<title>{pageProps.title} | RubberDuckStudying</title> :
						<title>RubberDuckStudying</title>
					}
					<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
					<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet" />
					<link rel="shortcut icon" type="image/x-icon" href="/static/favicon.ico" />
					<link rel="stylesheet" href="https://cdn.quilljs.com/1.2.6/quill.snow.css"></link>
					<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.css" crossOrigin="anonymous"></link>
					<script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.js" crossOrigin="anonymous"></script>
					<link rel="stylesheet" href="/static/styles/vs2015.css"></link>
					<script src="/static/js/highlight.pack.js"></script>
					<script>hljs.initHighlightingOnLoad();</script>
				</Head>
				<Provider store={reduxStore}>
					<ThemeProvider theme={theme}>
						<CssBaseline />
						<Component {...pageProps} />
					</ThemeProvider>
				</Provider >
			</>
		)
	}
}

export default withRematch(MyApp)
