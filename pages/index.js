import React from 'react';
import App from '../components/App';
import FooterSignUp from '../components/FooterSignUp';
import Hero from '../components/Hero';
import Overview from '../components/Overview';
import Process from '../components/Process';

function Index(props) {
	return (
		<App>
			<Hero />
			<Overview />
			<Process />
			<FooterSignUp />
		</App>
	)
}

Index.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default Index;
