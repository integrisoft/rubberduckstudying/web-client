import { List, ListItem, Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import App from '../../components/App';
import Link from '../../components/Link';
import NavBar from '../../components/NavBar';

const useStyles = makeStyles(theme => ({
	sectionTitle: {
		margin: '30px 0px',
		lineHeight: '1.3',
		fontSize: '24px',
	},
	sectionBody: {
		marginBottom: '18px',
		lineHeight: '1.6',
		fontSize: '16px',
	},
	container: {
		padding: '50px 20px',
	},
	list: {
		marginBottom: '25px',
	}
}));

function Privacy(props) {
	const classes = useStyles();
	return (
		<App>
			<NavBar />
			<Container maxWidth="md" className={classes.container}>
				<Typography variant='h4' className={classes.sectionTitle}>Privacy Policy</Typography>
				<Typography variant='body1' className={classes.sectionBody}>This privacy policy describes how RubberDuckStudying.com uses and protects any information that you give us. We are committed to ensuring that your privacy is protected. If you provide us with personal information through RubberDuckStudying.com, you can be assured that it will only be used in accordance with this privacy statement.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Website Visitors</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Like most website operators, RubberDuckStudying.com collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. RubberDuckStudying.com’s purpose in collecting non-personally identifying information is to better understand how RubberDuckStudying.com’s visitors use its website. From time to time, RubberDuckStudying.com may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>RubberDuckStudying.com also collects potentially personally-identifying information like Internet Protocol (IP) addresses. RubberDuckStudying.com does not use IP addresses to identify its visitors, however, and does not disclose such information, other than under the same circumstances that it uses and discloses personally-identifying information, as described below.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Gathering of Personally-Identifying Information</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Visitors to RubberDuckStudying.com choose to interact with RubberDuckStudying.com in ways that require RubberDuckStudying.com to gather personally-identifying information. For example, we ask visitors who sign up to RubberDuckStudying.com provide a username, email address, and display name.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>In each case, RubberDuckStudying.com collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor’s interaction with RubberDuckStudying.com. RubberDuckStudying.com does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>All of the information that is collected on RubberDuckStudying.com will be handled in accordance with GDPR legislation.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Protection of Certain Personally-Identifying Information</Typography>
				<Typography variant='body1' className={classes.sectionBody}>RubberDuckStudying.com discloses potentially personally-identifying and personally-identifying information only to those of project administrators, employees, contractors, and affiliated organizations that (i) need to know that information in order to process it on RubberDuckStudying.com’s behalf or to provide services available through RubberDuckStudying.com, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using RubberDuckStudying.com, you consent to the transfer of such information to them.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>RubberDuckStudying.com will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to project administrators, employees, contractors, and affiliated organizations, as described above, RubberDuckStudying.com discloses potentially personally-identifying and personally-identifying information only when required to do so by law, if you give permission to have your information shared, or when RubberDuckStudying.com believes in good faith that disclosure is reasonably necessary to protect the property or rights of RubberDuckStudying.com, third parties, or the public at large.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>If you are a registered user of a RubberDuckStudying.com website and have supplied your email address, RubberDuckStudying.com may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what’s going on with RubberDuckStudying.com and our products. We primarily use our blog to communicate this type of information, so we expect to keep this type of email to a minimum.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. RubberDuckStudying.com takes all measures reasonably necessary to protect against the unauthorized access, use, alteration, or destruction of potentially personally-identifying and personally-identifying information.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Use of personal information</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We use the information you provide to register for an account, attend our events, receive newsletters, use certain other services, or participate in the RubberDuckStudying platform in any other way.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We will not sell or lease your personal information to third parties unless we have your permission or are required by law to do so.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We would like to send you email marketing communication which may be of interest to you from time to time. If you have consented to marketing, you may opt out later.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>You have a right at any time to stop us from contacting you for marketing purposes. If you no longer wish to be contacted for marketing purposes, please click on the unsubscribe link at the bottom of the email.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Legal grounds for processing personal information</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We rely on one or more of the following processing conditions:</Typography>
				<List className={classes.list}>
					<ListItem divider ><Typography variant='body2'>our legitimate interests in the effective delivery of information and services to you;</Typography></ListItem>
					<ListItem divider ><Typography variant='body2'>explicit consent that you have given;</Typography></ListItem>
					<ListItem divider ><Typography variant='body2'>legal obligations.</Typography></ListItem>
				</List >
				<Typography variant='h4' className={classes.sectionTitle}>Access to data</Typography>
				<Typography variant='body1' className={classes.sectionBody}>You have the right to request a copy of the information we hold about you. If you would like a copy of some or all your personal information, please follow the instructions at the end of this section.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>RubberDuckStudying.com user accounts can be edited by following these steps:</Typography>
				<List className={classes.list}>
					<ListItem divider ><Typography variant='body2'>Visit <Link href='/account/profile'>https://RubberDuckStudying.com/account/profile</Link> to edit your account details.</Typography></ListItem>
				</List>
				<Typography variant='body1' className={classes.sectionBody}>If you would like to request access to your account data, please follow these steps:</Typography>

				<List className={classes.list}>
					<ListItem divider ><Typography variant='body2'>Send an email to support@RubberDuckStudying.com with a subject of "Data Export Request" and a body containing the email address and username of the account to export.</Typography></ListItem>
					<ListItem divider ><Typography variant='body2'>Note: We may ask you to verify that you are the owner of the account.</Typography></ListItem >
				</List >
				<Typography variant='h4' className={classes.sectionTitle}>Retention of personal information</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We will retain your personal information on our systems only for as long as we need to, for the success of the RubberDuckStudying platform. We keep contact information (such as mailing list information) until a user unsubscribes or requests that we delete that information from our live systems. If you choose to unsubscribe from a mailing list, we may keep certain limited information about you so that we may honor your request.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Any courses or tutorials created by the deleted account will be retained. However, all personally identifiable information will be removed and no longer associated with said courses and tutorials.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>When deletion is requested or otherwise required, we will anonymise the data of data subjects, including but not limited to courses and tutorials created by the deleted account, and/or remove their information from publicly accessible sites if the deletion of data would break essential systems or damage the logs or records necessary to the operation, development, or archival records of the RubberDuckStudying platform.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>If you would like to request deletion of your account and associated data, please follow these steps:</Typography>

				<List className={classes.list}>
					<ListItem divider ><Typography variant='body2'>Visit <Link href='/account/profile'>https://RubberDuckStudying.com/account/profile</Link>, and enter your username and password if requested.</Typography></ListItem>
					<ListItem divider ><Typography variant='body2'>At the bottom of the page, click the 'Deactivate Account' button and confirm when prompted.</Typography></ListItem >
				</List >
				<Typography variant='body1' className={classes.sectionBody}>You may have certain rights under data protection law in relation to the personal information we hold about you. In particular, you may have a right to:</Typography>

				<List className={classes.list}>
					<ListItem divider ><Typography variant='body2'>request a copy of personal information we hold about you;</Typography></ListItem>
					<ListItem divider ><Typography variant='body2'>ask that we update the personal information we hold about you, or independently correct such personal information that you think is incorrect or incomplete;</Typography></ListItem >
					<ListItem divider ><Typography variant='body2'>ask that we delete personal information that we hold about you from live systems, or restrict the way in which we use such personal information (for information on deletion from archives, see the “Retention of personal information” section)</Typography>;</ListItem >
					<ListItem divider ><Typography variant='body2'>object to our processing of your personal information; and/or</Typography></ListItem>
					<ListItem divider ><Typography variant='body2'>withdraw your consent to our processing of your personal information (to the extent such processing is based on consent and consent is the only permissible basis for processing).</Typography></ListItem>
				</List >
				<Typography variant='body1' className={classes.sectionBody}>If you would like to exercise these rights or understand if these rights apply to you, please follow the instructions at the end of this Privacy statement.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Third Party Links</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Our website may contain links to other websites provided by third parties not under our control. When following a link and providing information to a 3rd-party website, please be aware that we are not responsible for the data provided to that third party. This privacy policy only applies to the websites listed at the beginning of this document, so when you visit other websites, even when you click on a link posted on RubberDuckStudying.com, you should read their own privacy policies.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Aggregated Statistics</Typography>
				<Typography variant='body1' className={classes.sectionBody}>RubberDuckStudying.com may collect statistics about the behavior of visitors to its websites. For instance, RubberDuckStudying.com may reveal how many times a particular course or tutorial of RubberDuckStudying was visited or report on which tutorials are the most popular.. However, RubberDuckStudying.com does not disclose personally-identifying information other than as described in this policy.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Cookies</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Additionally, information about how you use our website is collected automatically using “cookies”. Cookies are text files placed on your computer to collect standard internet log information and visitor behaviour information. This information is provided by Google Analytics and is used to track visitor use of the website and to compile statistical reports on website activity.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Tracked information is anonymous and is never used to personally identify any individual user.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Privacy Policy Changes</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Although most changes are likely to be minor, RubberDuckStudying.com may change its Privacy Policy from time to time, and at RubberDuckStudying.com’s sole discretion. RubberDuckStudying.com encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Contact</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Please contact us if you have any questions about our privacy policy or information we hold about you by emailing support@RubberDuckStudying.com.</Typography>
				<a href='https://creativecommons.org/licenses/by-sa/4.0/'>
					<img
						src={'/static/images/by-sa.svg'}
						alt="Creative Commons"
						height={'32px'}
					/>
				</a>
			</Container >
		</App >
	)
}

Privacy.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default Privacy