import { makeStyles, Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import React from 'react';
import App from '../../components/App';
import NavBar from '../../components/NavBar';

const useStyles = makeStyles(theme => ({
	sectionTitle: {
		margin: '30px 0px',
		lineHeight: '1.3',
		fontSize: '24px',
	},
	sectionBody: {
		marginBottom: '18px',
		lineHeight: '1.6',
		fontSize: '16px',
	},
	container: {
		padding: '50px 20px',
	},
	list: {
		marginBottom: '25px',
	}
}));

function Terms(props) {
	const classes = useStyles();

	return (
		<App>
			<NavBar />
			<Container maxWidth="md" className={classes.container}>
				<Typography variant='h4' className={classes.sectionTitle}>Terms of Service ("Terms")</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Last updated: 1 September 2019</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Please read these Terms of Service ("Terms", "Terms of Service") carefully before using the https://rubberduckstudying.com website (the "Service") operated by Integrisoft LLC ("us", "we", or "our").</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Termination</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Content</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). In order to maintain a reliable source of information for all users of the platform, any courses, tutorials, and/or other original material created on the RubberDuckStudying platform will be retained even if the associated user account is deleted.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>In order to request the deletion of courses, tutorials, or other user generated material, please contact support@rubberduckstudying.com.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Links To Other Web Sites</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Our Service may contain links to third-party web sites or services that are not owned or controlled by Integrisoft LLC.</Typography>
				<Typography variant='body1' className={classes.sectionBody}>Integrisoft LLC has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Integrisoft LLC shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Changes</Typography>
				<Typography variant='body1' className={classes.sectionBody}>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</Typography>
				<Typography variant='h4' className={classes.sectionTitle}>Contact Us</Typography>
				<Typography variant='body1' className={classes.sectionBody}>If you have any questions about these Terms, please contact us at support@rubberduckstudying.com</Typography>
			</Container>
		</App >
	)
}

Terms.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default Terms