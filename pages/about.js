import { Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import App from '../components/App';
import NavBar from '../components/NavBar';

const useStyles = makeStyles(theme => ({
	root: {
		backgroundColor: '#f9f9f9',
		backgroundColor: theme.palette.background.black,
		background: 'url(https://raw.github.com/mmoustafa/Chalkboard/master/img/bg.png)',
	},
	header: {
		padding: '50px 0 50px 0',
		// padding: '30px 0px 30px 0px',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'row',
		alignContent: 'center',
		alignItems: 'center',
		width: '100%',
		textAlign: 'center',
		justifyContent: 'center',
	},
	headerText: {
		fontSize: '32px',
		marginBottom: '10px',
		color: '#fff',
		fontFamily: 'Chalky',
	},
	container: {
		paddingTop: '50px',
		// display: 'flex',
		// alignContent: 'center',
		// alignItems: 'center',
		// textAlign: 'center',
		// width: '100%',
		height: '100%',
		// backgroundColor: '#f00',
	},
	textTitle: {
		color: '#fff',
		fontFamily: 'Chalky',
		marginBottom: '20px',
	},
	textBody: {
		color: '#fff',
		fontFamily: 'Chalky',
		fontSize: '24px',
	},
}))

function About(props) {
	const classes = useStyles()
	return (
		<App>
			<NavBar />
			<main className={classes.root}>
				{/* <div className={classes.header}>
					<Typography className={classes.headerText} variant='h1'>About RubberDuckStudying</Typography>
				</div> */}
				<Container maxWidth="lg" className={classes.container}>
					<Typography className={classes.textTitle} variant='h1'>
						About RubberDuckStudying
					</Typography>
					<Typography className={classes.textBody} variant='body1'>
						RubberDuckStudying was created by Integrisoft LLC with the goal of
						giving people of all ages and backgrounds the confidence and skills to independently learn anything they desire.
					</Typography>
					<Typography className={classes.textBody} variant='body1'>
						Over 3 billion people have some access to the internet.
					</Typography>
				</Container>
			</main>
		</App>
	)
}

About.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default About