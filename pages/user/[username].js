import { Container } from '@material-ui/core';
import React from 'react';
import App from '../../components/App';
import ErrorHandler from '../../components/ErrorHandler';
import NavBar from '../../components/NavBar';
import NotFound from '../../components/NotFound';
import UserViewer from '../../components/UserViewer';
import initApollo from '../../shared/initApollo';
import { GET_USER } from '../../shared/models/user';
import withApollo from '../../shared/withApollo';
import { withAuthSync } from '../../shared/withAuth';

function User({ user, error }) {
	``
	if (error) {
		return <ErrorHandler error={error} />
	}

	if (!user) {
		return <NotFound />
	}

	return (
		<App>
			<NavBar />
			<Container maxWidth='md'>
				<UserViewer user={user} />
			</Container>
		</App >
	)
}

User.getInitialProps = async function ({ req, query, reduxStore }) {

	let cookie = undefined
	if (req) {
		cookie = req.headers.cookie
	}
	let client = initApollo({}, cookie)
	try {
		let getUserResponse = client.query({
			query: GET_USER,
			variables: {
				where: {
					userID: query.username,
				},
			},
		})
		await reduxStore.dispatch.user.loadProfile({
			cookie: cookie
		})
		const response = await getUserResponse
		return {
			user: response.data.user,
			error: response.error
		}
	} catch (err) {
		return {
			user: {},
			error: err
		}
	}
}

export default withApollo(withAuthSync(User));
