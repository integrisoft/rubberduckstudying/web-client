import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Link from '../../../components/Link';

function VerifyEmail(props) {
	const router = useRouter();

	useEffect(() => {
		props.verifyEmail({ token: router.query.token })
	}, []) // passing an empty array as second argument triggers the callback in useEffect only after the initial render thus replicating `componentDidMount` lifecycle behaviour
	return (
		<div>
			If you are not automatically redirected, <Link href='/' > click here to return home</Link>
		</div>
	)
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = ({ user: { verifyEmail } }) => ({
	verifyEmail: (token) => verifyEmail(token),
})

VerifyEmail.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(VerifyEmail);

