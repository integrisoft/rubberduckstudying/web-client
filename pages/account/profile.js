import Container from '@material-ui/core/Container';
import React from 'react';
import App from '../../components/App';
import NavBar from '../../components/NavBar';
import ProfileForm from '../../components/Profile';
import { withAuthSync } from '../../shared/withAuth';

function Profile(props) {
	return (
		<App>
			<NavBar />
			<Container maxWidth="md">
				<ProfileForm />
			</Container>
		</App >
	)
}

Profile.getInitialProps = async ({ req, reduxStore }) => {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default withAuthSync(Profile);

