import { default as React } from 'react';
import App from '../../../../../../components/App';
import ErrorHandler from '../../../../../../components/ErrorHandler';
import NavBar from '../../../../../../components/NavBar';
import NotFound from '../../../../../../components/NotFound';
import TutorialViewer from '../../../../../../components/TutorialViewer';
import initApollo from '../../../../../../shared/initApollo';
import { GET_TUTORIAL } from '../../../../../../shared/models/tutorial';
import withApollo from '../../../../../../shared/withApollo';
import { withAuthSync } from '../../../../../../shared/withAuth';

function Tutorial({ tutorial, error }) {
	if (error) {
		return <ErrorHandler error={error} />
	}

	if (!tutorial) {
		return <NotFound />
	}

	if (!tutorial.parentGoal.parentUnit.parentCourse.courseRole.viewCourse) {
		return <Unauthorized />
	}

	return (
		<App>
			<NavBar />
			<TutorialViewer tutorial={tutorial} />
		</App >
	)
}

Tutorial.getInitialProps = async function ({ req, query, reduxStore }) {
	let cookie = undefined
	if (req) {
		cookie = req.headers.cookie
	}
	let client = initApollo({}, cookie)
	try {
		let getTutorialResponse = await client.query({
			query: GET_TUTORIAL,
			variables: {
				where: {
					id: query.id,
				},
			},
		})
		await reduxStore.dispatch.user.loadProfile({
			cookie: cookie
		})
		const response = await getTutorialResponse
		return {
			tutorial: response.data.tutorial,
			error: response.error
		}
	} catch (err) {
		return {
			tutorial: {},
			error: err
		}
	}
}

export default withApollo(withAuthSync(Tutorial));
