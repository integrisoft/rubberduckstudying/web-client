import { useRouter } from 'next/router';
import React from 'react';
import { useQuery } from 'react-apollo';
import App from '../../../../../../components/App';
import ErrorHandler from '../../../../../../components/ErrorHandler';
import Loading from '../../../../../../components/Loading';
import NavBar from '../../../../../../components/NavBar';
import NotFound from '../../../../../../components/NotFound';
import TutorialCreateForm from '../../../../../../components/TutorialCreateForm';
import Unauthorized from '../../../../../../components/Unauthorized';
import { GET_GOAL } from '../../../../../../shared/models/goal';
import withApollo from '../../../../../../shared/withApollo';
import { withAuthSync } from '../../../../../../shared/withAuth';

function CreateTutorial(props) {
	const router = useRouter();

	const { data, error } = useQuery(GET_GOAL, {
		variables: {
			where: {
				courseSlug: router.query.slug,
				unitSlug: router.query.unit,
				goalSlug: router.query.goal,
			}
		},
		notifyOnNetworkStatusChange: true,
	});

	if (error) {
		return <ErrorHandler error={error} />
	}

	if (!data) {
		return <Loading />;
	}

	if (!data.goal) {
		return <NotFound />
	}

	if (!data.goal.parentUnit.parentCourse.courseRole.viewCourse) {
		return <Unauthorized />
	}

	return (
		<App>
			<NavBar />
			<TutorialCreateForm goal={data.goal} />
		</App >
	)
}

CreateTutorial.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default withApollo(withAuthSync(CreateTutorial));
