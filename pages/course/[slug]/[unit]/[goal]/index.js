import React from 'react';
import App from '../../../../../components/App';
import ErrorHandler from '../../../../../components/ErrorHandler';
import GoalViewer from '../../../../../components/GoalViewer';
import NavBar from '../../../../../components/NavBar';
import NotFound from '../../../../../components/NotFound';
import initApollo from '../../../../../shared/initApollo';
import { GET_GOAL } from '../../../../../shared/models/goal';
import withApollo from '../../../../../shared/withApollo';
import { withAuthSync } from '../../../../../shared/withAuth';

function Goal({ goal, error }) {
	if (error) {
		return <ErrorHandler error={error} />
	}

	if (!goal) {
		return <NotFound />
	}

	if (!goal.parentUnit.parentCourse.courseRole.viewCourse) {
		return <Unauthorized />
	}

	return (
		<App>
			<NavBar />
			<GoalViewer goal={goal} />
		</App >
	)
}

Goal.getInitialProps = async function ({ req, query, reduxStore }) {
	let cookie = undefined
	if (req) {
		cookie = req.headers.cookie
	}
	let client = initApollo({}, cookie)
	try {
		let getGoalResponse = client.query({
			query: GET_GOAL,
			variables: {
				where: {
					courseSlug: query.slug,
					unitSlug: query.unit,
					goalSlug: query.goal,
				},
			},
		})
		await reduxStore.dispatch.user.loadProfile({
			cookie: cookie
		})
		const response = await getGoalResponse
		return {
			goal: response.data.goal,
			error: response.error
		}
	} catch (err) {
		return {
			goal: {},
			error: err
		}
	}
}

export default withApollo(withAuthSync(Goal));
