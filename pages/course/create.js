import React from 'react';
import App from '../../components/App';
import CourseCreateForm from '../../components/CourseCreateForm';
import ErrorHandler from '../../components/ErrorHandler';
import NavBar from '../../components/NavBar';
import initApollo, { withApollo } from '../../shared/initApollo';
import { GET_TAGS } from '../../shared/models/tag';
import { withAuthSync } from '../../shared/withAuth';

function CreateCourse({ tags, error }) {
	if (error) {
		return <ErrorHandler error={error} />
	}
	return (
		<App>
			<NavBar />
			<CourseCreateForm allTags={tags} />
		</App >
	)
}

CreateCourse.getInitialProps = async function ({ req, reduxStore }) {
	let cookie = undefined
	const title = 'Create Course'
	if (req) {
		cookie = req.headers.cookie
	}
	let client = initApollo({}, cookie)
	try {
		let getTagsResponse = await client.query({
			query: GET_TAGS,
		})
		await reduxStore.dispatch.user.loadProfile({
			cookie: cookie
		})
		const response = await getTagsResponse
		return {
			tags: response.data.tags,
			error: response.error,
			title
		}
	} catch (err) {
		return {
			tags: [],
			error: err,
			title
		}
	}
}

export default withApollo(withAuthSync(CreateCourse));
