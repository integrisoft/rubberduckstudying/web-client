import { Typography } from '@material-ui/core';
import React from 'react';
import App from '../../../components/App';
import CourseCreateForm from '../../../components/CourseCreateForm';
import Loading from '../../../components/Loading';
import NavBar from '../../../components/NavBar';
import Unauthorized from '../../../components/Unauthorized';
import initApollo from '../../../shared/initApollo';
import { GET_COURSE } from '../../../shared/models/course';
import { GET_TAGS } from '../../../shared/models/tag';
import withApollo from '../../../shared/withApollo';
import { withAuthSync } from '../../../shared/withAuth';

function EditCourse({ course, error, tags }) {

	if (error) {
		return <Typography variant='body1'>Error loading course</Typography>
	}
	if (!course) {
		return <Loading />;
	}

	if (!course.courseRole.manageCourse) {
		return <Unauthorized />
	}

	return (
		<App>
			<NavBar />
			<CourseCreateForm allTags={tags} course={course} editMode={true} />
		</App >
	)
}


EditCourse.getInitialProps = async function ({ req, query, reduxStore }) {
	let cookie = undefined
	const title = 'Update Course'
	if (req) {
		cookie = req.headers.cookie
	}
	let client = initApollo({}, cookie)
	try {
		let getTagsResponse = client.query({ query: GET_TAGS })
		let getCourseResponse = await client.query({
			query: GET_COURSE,
			variables: {
				where: {
					slug: query.slug,
				},
			},
		})
		await reduxStore.dispatch.user.loadProfile({
			cookie: cookie
		})
		const response = await getCourseResponse
		const tagsResponse = await getTagsResponse
		return {
			course: response.data.course,
			error: response.error,
			tags: tagsResponse.data.tags,
			title
		}
	} catch (err) {
		return {
			course: {},
			error: err,
			tags: [],
			title,
		}
	}
}

export default withApollo(withAuthSync(EditCourse));
