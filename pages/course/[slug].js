import React from 'react';
import App from '../../components/App';
import CourseViewer from '../../components/CourseViewer';
import ErrorHandler from '../../components/ErrorHandler';
import NavBar from '../../components/NavBar';
import NotFound from '../../components/NotFound';
import Unauthorized from '../../components/Unauthorized';
import initApollo from '../../shared/initApollo';
import { GET_COURSE } from '../../shared/models/course';
import withApollo from '../../shared/withApollo';
import { withAuthSync } from '../../shared/withAuth';


function Course({ course, error }) {
	if (error) {
		return <ErrorHandler error={error} />
	}

	if (!course) {
		return <NotFound />
	}

	if (!course.courseRole.viewCourse) {
		return <Unauthorized />
	}

	return (
		<App>
			<NavBar />
			<CourseViewer course={course} />
		</App >
	)
}

Course.getInitialProps = async function ({ req, query, reduxStore }) {
	let cookie = undefined
	const title = 'My New Course'
	if (req) {
		cookie = req.headers.cookie
	}
	let client = initApollo({}, cookie)
	try {
		let getCourseResponse = await client.query({
			query: GET_COURSE,
			variables: {
				where: {
					slug: query.slug,
				},
			},
		})
		await reduxStore.dispatch.user.loadProfile({
			cookie: cookie
		})
		const response = await getCourseResponse
		return {
			course: response.data.course,
			error: response.error,
			title
		}
	} catch (err) {
		return {
			course: {},
			error: err,
			title,
		}
	}
}

export default withApollo(withAuthSync(Course));
