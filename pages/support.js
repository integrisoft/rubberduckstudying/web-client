import Container from '@material-ui/core/Container';
import React from 'react';
import App from '../components/App';
import NavBar from '../components/NavBar';
import PageSpinner from '../components/PageSpinner';

function Support(props) {
	return (
		<App>
			<NavBar />
			<Container maxWidth="sm">
				<p>Contact us at support@rubberduckstudying.com</p>
				<PageSpinner />
			</Container>
		</App >
	)
}

Support.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default Support