import React from 'react';
import ErrorHandler from '../components/ErrorHandler';

function Error({ statusCode }) {
	return (
		<ErrorHandler error={{ code: statusCode }} />
	)
}

Error.getInitialProps = ({ res, err }) => {
	const statusCode = res ? res.statusCode : err ? err.statusCode : null
	return { statusCode }
}

export default Error