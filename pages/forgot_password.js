import { Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import App from '../components/App';
import ForgotPasswordForm from '../components/ForgotPasswordForm';
import Link from '../components/Link';
import withApollo from '../shared/withApollo';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '50px 20px 50px 20px',
		backgroundColor: theme.palette.primary.main,
		height: '100vh',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
	},
	signupMessage: {
		marginTop: '20px',
		color: '#eee',
	},
	signupLink: {
		color: 'white',
		textDecoration: 'underline',
	},
}));

function ForgotPassword(props) {
	const classes = useStyles();
	return (
		<App >
			<div className={classes.root}>
				<Container maxWidth="sm">
					<ForgotPasswordForm />
					<Typography variant='subtitle1' className={classes.signupMessage}>Don't have an account? <Link href='/signup' className={classes.signupLink}>Create one now</Link></Typography>
				</Container>
			</div>
		</App>
	)
}

ForgotPassword.getInitialProps = async function ({ req, reduxStore }) {
	await reduxStore.dispatch.user.loadProfile({
		cookie: req.headers.cookie
	})
}

export default withApollo(ForgotPassword)