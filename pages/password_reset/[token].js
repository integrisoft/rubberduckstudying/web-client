import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useRouter } from 'next/router';
import React from 'react';
import App from '../../components/App';
import PasswordResetForm from '../../components/PasswordResetForm';
import withApollo from '../../shared/withApollo';

const useStyles = makeStyles(theme => ({
	root: {
		textAlign: 'center',
		padding: '50px 20px 50px 20px',
		backgroundColor: theme.palette.primary.main,
		height: '100vh',
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		alignContent: 'center',
	},
	signupMessage: {
		marginTop: '20px',
		color: '#eee',
	},
	signupLink: {
		color: 'white',
		textDecoration: 'underline',
	},
}));

function ForgotPassword(props) {
	const classes = useStyles();
	const router = useRouter();
	return (
		<App >
			<div className={classes.root}>
				<Container maxWidth="sm">
					<PasswordResetForm token={router.query.token} />
				</Container>
			</div>
		</App>
	)
}

ForgotPassword.getInitialProps = async function ({ req, reduxStore }) {
	if (req) {
		await reduxStore.dispatch.user.loadProfile({
			cookie: req.headers.cookie
		})
	} else {
		await reduxStore.dispatch.user.loadProfile()
	}
	return {}
}

export default withApollo(ForgotPassword)