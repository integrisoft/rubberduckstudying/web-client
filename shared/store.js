import { init } from '@rematch/core';
import { course, goal, signup, statusMessage, tutorial, unsplash, user } from './models';

const exampleInitialState = {
	user: {
		currentUser: null,
		loggedIn: false,
		profileLoaded: false,
	},
	signup: {
		usernameIsUnique: true,
	},
	statusMessage: {
		status: null,
	},
}

export const initializeStore = (initialState = exampleInitialState) =>
	init({
		models: {
			user,
			signup,
			statusMessage,
			course,
			goal,
			tutorial,
			unsplash,
		},
		redux: {
			initialState
		}
	})