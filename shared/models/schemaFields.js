export const roleFields = [
	'isMember',
	'viewUser',
	'manageUser',
	'deleteUser',
	'viewSpace',
	'manageSpace',
	'deleteSpace',
	'viewCourse',
	'manageCourse',
	'deleteCourse',
	'viewTutorial',
	'manageTutorial',
	'deleteTutorial',
	'deleteComment'
]

export const photoFields = [
	'id',
	`images {
		raw,
		full,
		regular,
		small,
		thumb
	}`,
	`links {
		self,
		html,
		download,
		downloadLocation
	}`,
	`user {
		id,
		username,
		name,
		firstName,
		lastName,
		twitterUsername,
		portfolioUrl,
		bio,
		location,
		links {
			self,
			html,
			photos,
			likes,
			portfolio,
			following,
			followers
		}
	}`
]

export const courseFields = [
	'id',
	'name',
	'slug',
	'description',
	'tags',
	'unsplashImageId', `unsplashPhoto{${photoFields.join(',')}}`, 'users{id}',
	'space{id, name, description}',
	'units{id, slug, name, description, goals{id, slug, name, description, tutorials{id}}}',
	`courseRole{${roleFields.join(',')}}`,
	'createdAt',
	'updatedAt'
]

export const courseFieldsNoImage = [
	'id',
	'name',
	'slug',
	'description',
	'space{id, name, description}',
	'units{id, slug, name, description, goals{id, slug, name, description, tutorials{id}}}',
	`courseRole{${roleFields.join(',')}}`,
	'createdAt',
	'updatedAt'
]

export const goalFields = [
	'id',
	'slug',
	'name',
	'description',
	'createdAt',
	'updatedAt',
	`parentUnit{name, slug, description, parentCourse {${courseFieldsNoImage.join(',')}}}`,
	'tutorials{ id, title, content, editCount, createdAt, updatedAt, author{ id, username, bio }, stars{id, username}}',
]

export const tutorialFields = [
	'id',
	'title',
	'content',
	'editCount',
	'createdAt',
	'updatedAt',
	`author{id, username, bio}`,
	`parentGoal{${goalFields.join(',')}}`,
	'stars{id, username}',
]

export const userFields = [
	'id',
	'email',
	'username',
	'bio',
	'name',
	'isAdmin',
	'emailVerified',
	'createdAt',
	'updatedAt'
]

export const userFieldsWithCourses = [
	'id',
	'email',
	'username',
	'bio',
	'name',
	'isAdmin',
	'emailVerified',
	'createdAt',
	'updatedAt',
	`courses{${courseFields.join(',')}}`,
]
