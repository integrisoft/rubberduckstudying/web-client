import gql from 'graphql-tag';
import { photoFields } from './schemaFields';

export const referralParameters = '?utm_source=rubberduckstudying&utm_medium=referral'

export const GET_PHOTO = gql`
  query unsplashPhoto($id: String!) {
    unsplashPhoto(id: $id) {
			${photoFields.join(',')}
		}
  }
`

export const GET_RANDOM_PHOTO = gql`
  query unsplashRandomPhoto {
    unsplashRandomPhoto {
			${photoFields.join(',')}
		}
  }
`

const unsplash = {
	state: {
		selectedPhoto: null,
		searchQuery: "",
	}, // initial state
	reducers: {
		setSelectedPhoto(state, payload) {
			return {
				...state,
				selectedPhoto: payload,
			}
		},
		setSearchQuery(state, payload) {
			return {
				...state,
				searchQuery: payload,
			}
		},
	},
	effects: dispatch => ({

	}),
}

export default unsplash