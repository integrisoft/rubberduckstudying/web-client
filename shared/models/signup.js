import gql from 'graphql-tag';
import initApollo from '../initApollo';

const USERNAME_IS_UNIQUE = gql`
  query usernameIsUnique($username: String!) {
    usernameIsUnique(username: $username)
  }
`

const signup = {
	state: {
		usernameIsUnique: true,
	}, // initial state
	reducers: {
		setUsernameIsUnique(state, payload) {
			return {
				...state,
				usernameIsUnique: payload,
			}
		},
	},
	effects: dispatch => ({
		// handle state changes with impure functions.
		// use async/await for async actions
		usernameIsUnique: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.query({
					query: USERNAME_IS_UNIQUE,
					variables: {
						username: payload,
					},
				})
				dispatch.signup.setUsernameIsUnique(response.data.usernameIsUnique)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
				dispatch.signup.setUsernameIsUnique(false)
			}
		},
	})
}

export default signup