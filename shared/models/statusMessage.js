const statusMessage = {
	state: {
		status: null,
	}, // initial state
	reducers: {
		setStatus(state, payload) {
			return {
				...state,
				status: { variant: payload.variant, message: payload.message, date: new Date() },
			}
		},
		clearStatus(state, payload) {
			return {
				...state,
				status: null,
			}
		},
	},
}

export default statusMessage