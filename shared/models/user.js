import gql from 'graphql-tag';
import Router from 'next/router';
import initApollo from '../initApollo';
import { getGqlErrors } from '../utils';
import { userFields, userFieldsWithCourses } from './schemaFields';

const CREATE_USER = gql`
  mutation createUser($email: String!, $username: String!, $password: String!, $name: String!) {
    createUser(data:{
			email: $email,
			username: $username,
			password: $password,
			name: $name
			}) {
				${userFields.join(',')}
    }
  }
`

const UPDATE_CURRENT_USER = gql`
  mutation updateCurrentUser($name: String!, $bio: String!, $username: String!) {
    updateCurrentUser(data:{
			name: $name,
			bio: $bio,
			username: $username
			}) {
				${userFields.join(',')}
    }
  }
`

const DELETE_CURRENT_USER = gql`
  mutation deleteCurrentUser {
    deleteCurrentUser {
				${userFields.join(',')}
    }
  }
`

const CHANGE_EMAIL = gql`
  mutation changeEmail($currentPassword: String!, $newEmail: String!) {
    changeEmail(currentPassword: $currentPassword, newEmail: $newEmail) {
				${userFields.join(',')}
    }
  }
`

const CHANGE_PASSWORD = gql`
  mutation changePassword($currentPassword: String!, $newPassword: String!) {
    changePassword(currentPassword: $currentPassword, newPassword: $newPassword) {
				${userFields.join(',')}
    }
  }
`

const RESEND_VERIFICATION_EMAIL = gql`
  mutation resendVerificationEmail($userID: String!) {
    resendVerificationEmail(userID: $userID)
  }
`

const VERIFY_EMAIL = gql`
  mutation verifyEmail($token: String!) {
    verifyEmail(token: $token) {
			${userFields.join(',')}
		}
  }
`

const FORGOT_PASSWORD = gql`
  mutation forgotPassword($email: String!) {
    forgotPassword(email: $email)
  }
`

const RESET_PASSWORD = gql`
  mutation resetPassword($token: String!, $newPassword: String!) {
    resetPassword(token: $token, newPassword: $newPassword)
  }
`

const LOAD_USER_PROFILE = gql`
  query profile {
    profile {
      ${userFields.join(',')}
    }
  }
`

export const GET_USER = gql`
  query user($where: UserWhereUniqueInput!) {
    user(where: $where) {
      ${userFieldsWithCourses.join(',')}
    }
  }
`

const LOGIN = gql`
  mutation login($userID: String!, $password: String!) {
    login(userID: $userID, password: $password) {
			${userFields.join(',')}
    }
  }
`

const LOGOUT = gql`
  mutation logout {
    logout
  }
`

const user = {
	state: {
		currentUser: null,
		loggedIn: false,
		profileLoaded: false,
	}, // initial state
	reducers: {
		setCurrentUser(state, payload) {
			return {
				...state,
				currentUser: payload != null ? payload : null,
				loggedIn: payload != null ? true : false,
			}
		},
		setProfileLoaded(state, payload) {
			return {
				...state,
				profileLoaded: payload,
			}
		},
	},
	effects: dispatch => ({
		// handle state changes with impure functions.
		// use async/await for async actions
		createUser: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: CREATE_USER,
					variables: {
						email: payload.email,
						username: payload.username,
						password: payload.password,
						name: payload.name,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `An email has been sent to ${payload.email}. Follow the link inside to finish verifying your email.` })
				dispatch.user.setCurrentUser(response.data.createUser)
				Router.push('/login')
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
				dispatch.user.setCurrentUser(null)
			}
		},
		updateCurrentUser: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: UPDATE_CURRENT_USER,
					variables: {
						name: payload.name,
						bio: payload.bio,
						username: payload.username,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: 'User updated successfully.' })
				dispatch.user.setCurrentUser(response.data.updateCurrentUser)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		deleteCurrentUser: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: DELETE_CURRENT_USER,
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: 'Account deleted.' })
				await client.resetStore()
				dispatch.user.setCurrentUser(null)
				Router.push('/')
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		changeEmail: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: CHANGE_EMAIL,
					variables: {
						currentPassword: payload.currentPassword,
						newEmail: payload.newEmail,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `An email has been sent to ${payload.newEmail}. Follow the link inside to finish changing your email.` })
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		changePassword: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: CHANGE_PASSWORD,
					variables: {
						currentPassword: payload.currentPassword,
						newPassword: payload.newPassword,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: 'Password has been changed.' })
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		forgotPassword: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: FORGOT_PASSWORD,
					variables: {
						email: payload,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `An email has been sent to ${payload}. Follow the link inside to change your password.` })
				Router.push('/login')
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		resetPassword: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: RESET_PASSWORD,
					variables: {
						token: payload.token,
						newPassword: payload.newPassword,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				Router.push('/login')
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		resendVerificationEmail: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: RESEND_VERIFICATION_EMAIL,
					variables: {
						userID: payload.userID,
					},
				})
				dispatch.statusMessage.setStatus({ variant: 'success', message: `An email has been sent to ${rootState.user.currentUser.email}. Follow the link inside to finish verifying your email.` })
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		verifyEmail: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: VERIFY_EMAIL,
					variables: {
						token: payload.token,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: 'Email verified.' })
				dispatch.user.setCurrentUser(response.data.verifyEmail)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
			Router.push('/login')
		},
		loadProfile: async (payload, rootState) => {
			let client = initApollo(rootState, payload && payload.cookie)
			dispatch.user.setProfileLoaded(false)
			try {
				let response = await client.query({
					query: LOAD_USER_PROFILE,
					fetchPolicy: 'network-only',
				})
				// If there was an error, just set it to null
				dispatch.user.setCurrentUser(response.data.profile)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
				dispatch.user.setCurrentUser(null)
			}
			dispatch.user.setProfileLoaded(true)
		},
		login: async (payload, rootState) => {

			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: LOGIN,
					variables: {
						userID: payload.userID,
						password: payload.password,
					},
					options: {
						refetchQueries: [{ query: LOAD_USER_PROFILE }],
						awaitRefetchQueries: true,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.user.setCurrentUser(response.data.login)
				if (typeof payload.redirectTo !== 'undefined') {
					Router.push(payload.redirectTo)
				}
				else {
					Router.push('/dashboard')
				}
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
				dispatch.user.setCurrentUser(null)
			}
		},
		logout: async (payload, rootState) => {
			let client = initApollo()
			try {
				await client.mutate({
					mutation: LOGOUT,
				})
				await client.resetStore()
				dispatch.user.setCurrentUser(null)
				Router.push('/')
				dispatch.statusMessage.setStatus({ variant: 'success', message: 'Logged out' })
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
				dispatch.user.setCurrentUser(null)
			}
		},
	})
}

export default user