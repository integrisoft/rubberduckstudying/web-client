import gql from 'graphql-tag';
import Router from 'next/router';
import initApollo from '../initApollo';
import { getGqlErrors } from '../utils';
import { courseFields } from './schemaFields';

const CREATE_COURSE = gql`
	mutation createCourse($name: String!, $description: String!, $space: SpaceWhereUniqueInput!, $unsplashImageId: String!, $units: [UnitCreateWithoutCourseInput!], $tags: [String!]) {
    createCourse(data: {
			name: $name,
			description: $description,
			units: $units,
			space: $space,
			unsplashImageId: $unsplashImageId,
			tags: $tags,
		}) {
			${courseFields.join(',')}
		}
  }
`

const UPDATE_COURSE = gql`
	mutation updateCourse($slug: String, $name: String, $description: String, $unsplashImageId: String, $units: [UnitUpdateInput!], $tags: [String!]) {
    updateCourse(where: {
			slug: $slug
			}, data: {
			name: $name,
			description: $description,
			units: $units,
			unsplashImageId: $unsplashImageId,
			tags: $tags,
		}) {
			${courseFields.join(',')}
		}
  }
`

export const GET_COURSE = gql`
  query course($where: CourseWhereUniqueInput!) {
    course(where: $where) {
			${courseFields.join(',')}
  	}
	}
`;

const JOIN_COURSE = gql`
  mutation joinCourse($where: CourseWhereUniqueInput!) {
    joinCourse(where: $where) {
			${courseFields.join(',')}
  	}
	}
`;

const LEAVE_COURSE = gql`
  mutation leaveCourse($where: CourseWhereUniqueInput!) {
    leaveCourse(where: $where) {
			${courseFields.join(',')}
  	}
	}
`;


const course = {
	state: {
	}, // initial state
	reducers: {
	},
	effects: dispatch => ({
		// handle state changes with impure functions.
		// use async/await for async actions
		createCourse: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: CREATE_COURSE,
					variables: {
						name: payload.name,
						description: payload.description,
						unsplashImageId: payload.unsplashImageId,
						units: payload.units,
						space: {
							slug: "public",
						},
						tags: payload.tags,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Successfully created course.` })
				Router.push(`/course/${response.data.createCourse.slug}`)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		updateCourse: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: UPDATE_COURSE,
					variables: {
						slug: payload.slug,
						name: payload.name,
						description: payload.description,
						unsplashImageId: payload.unsplashImageId,
						units: payload.units,
						tags: payload.tags,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Successfully updated course.` })
				Router.push(`/course/${response.data.updateCourse.slug}`)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		joinCourse: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: JOIN_COURSE,
					variables: {
						where: {
							id: payload.id,
						},
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Joined course.` })
				// Router.push(`/course/${response.data.joinCourse.slug}`)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		leaveCourse: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: LEAVE_COURSE,
					variables: {
						where: {
							id: payload.id,
						},
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Left course.` })
				// Router.push(`/course/${response.data.joinCourse.slug}`)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
	})
}

export default course