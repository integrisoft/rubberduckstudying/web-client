import gql from 'graphql-tag';
import initApollo from '../initApollo';
import { getGqlErrors } from '../utils';
import { goalFields } from './schemaFields';

const UPDATE_GOAL = gql`
	mutation updateGoal($id: String, $name: String, $description: String) {
    updateGoal(where: {
			id: $id
			}, data: {
			name: $name,
			description: $description,
		}) {
			${goalFields.join(',')}
		}
  }
`

export const GET_GOAL = gql`
	query goal($where: GoalWhereUniqueInput!) {
    goal(where: $where) {
			${goalFields.join(',')}
  	}
	}
`;


const goal = {
	state: {
	}, // initial state
	reducers: {
	},
	effects: dispatch => ({
		// handle state changes with impure functions.
		// use async/await for async actions
		updateGoal: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: UPDATE_GOAL,
					variables: {
						id: payload.id,
						name: payload.name,
						description: payload.description,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Successfully updated goal.` })
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
	})
}

export default goal