import gql from 'graphql-tag';
import Router from 'next/router';
import initApollo from '../initApollo';
import { getGqlErrors } from '../utils';
import { tutorialFields } from './schemaFields';

const CREATE_TUTORIAL = gql`
	mutation createTutorial($goal: GoalWhereUniqueInput!, $title: String!, $content: String!) {
    createTutorial(data: {
			title: $title,
			content: $content,
			goal: $goal,
		}) {
			${tutorialFields.join(',')}
		}
  }
`

const UPDATE_TUTORIAL = gql`
	mutation updateTutorial($id: String, $title: String, $content: String) {
    updateTutorial(where: {
			id: $id
			}, data: {
			title: $title,
			content: $content,
		}) {
			${tutorialFields.join(',')}
		}
  }
`

const TOGGLE_STAR = gql`
	mutation toggleStar($where: TutorialWhereUniqueInput!) {
    toggleStar(where: $where) {
			${tutorialFields.join(',')}
		}
  }
`

export const GET_TUTORIAL = gql`
	query tutorial($where: TutorialWhereUniqueInput!) {
    tutorial(where: $where) {
			${tutorialFields.join(',')}
  	}
	}
`;

const tutorial = {
	state: {
	}, // initial state
	reducers: {
	},
	effects: dispatch => ({
		// handle state changes with impure functions.
		// use async/await for async actions
		createTutorial: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: CREATE_TUTORIAL,
					variables: {
						goal: {
							id: payload.goal.id,
						},
						title: payload.title,
						content: payload.content,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Successfully created tutorial.` })
				Router.push(`/course/${payload.goal.parentUnit.parentCourse.slug}/${payload.goal.parentUnit.slug}/${payload.goal.slug}/tutorial/${response.data.createTutorial.id}`)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		updateTutorial: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: UPDATE_TUTORIAL,
					variables: {
						id: payload.tutorialID,
						title: payload.title,
						content: payload.content,
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
				dispatch.statusMessage.setStatus({ variant: 'success', message: `Successfully updated tutorial.` })
				Router.push(`/course/${response.data.updateTutorial.parentGoal.parentUnit.parentCourse.slug}/${response.data.updateTutorial.parentGoal.parentUnit.slug}/${response.data.updateTutorial.parentGoal.slug}/tutorial/${response.data.updateTutorial.id}`)
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
		toggleStar: async (payload, rootState) => {
			let client = initApollo()
			try {
				let response = await client.mutate({
					mutation: TOGGLE_STAR,
					variables: {
						where: {
							id: payload.tutorialID,
						}
					},
				})
				if (response.errors) {
					dispatch.statusMessage.setStatus({ variant: 'error', message: getGqlErrors(response.errors) })
					return
				}
			} catch (err) {
				dispatch.statusMessage.setStatus({ variant: 'error', message: err.toString() })
			}
		},
	})
}

export default tutorial