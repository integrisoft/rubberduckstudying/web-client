import * as moment from 'moment';

export function formatUtcDate(utcDate) {
	return moment.utc(utcDate).local().format('MMM D, YYYY')
}
