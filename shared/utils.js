import { get } from 'lodash';

// Robust way to check if it's Node or browser
export const checkServer = () => {
	return typeof window === 'undefined'
}

export const getGqlErrors = error =>
	(error || [])
		.map(err => get(err, 'extensions.message', get(err, 'message', '')))
		.join('. ')
