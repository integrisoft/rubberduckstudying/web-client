import nextCookie from 'next-cookies';
import { Component } from 'react';

export const auth = ctx => {
	const { user } = nextCookie(ctx)
	if (ctx.req && !user) {
		ctx.res.writeHead(302, { Location: '/login?to=' + ctx.req.url })
		ctx.res.end()
		return
	}

	// if (!token) {
	// 	ctx.res.writeHead(302, { Location: '/login' })
	// 	// Router.push('/login')
	// }

	return user
}

// Gets the display name of a JSX component for dev tools
const getDisplayName = Component =>
	Component.displayName || Component.name || 'Component'

export const withAuthSync = WrappedComponent =>
	class extends Component {
		static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`

		static async getInitialProps(ctx) {

			const token = auth(ctx)

			const componentProps =
				WrappedComponent.getInitialProps &&
				(await WrappedComponent.getInitialProps(ctx))

			return { ...componentProps, token }
		}

		render() {
			return <WrappedComponent {...this.props} />
		}
	}
