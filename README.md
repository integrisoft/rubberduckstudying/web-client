# RubberDuckStudying Web Client

## Requirements

-   yarn
-   node@10.15.0

## Setup

-   Install nvm following the instructions [here](https://github.com/creationix/nvm#verify-installation)
-   Install node version 10.15.0
-       nvm install 10.15.0
-   Install yarn
-       npm install -g yarn
-   Clone the repository
-       git clone git@gitlab.com:integrisoft/rubberduckstudying/web-client.git
-   Install missing dependencies
-       yarn
-   Start the web development client
-       yarn dev

Install EditorConfig in your text editor

## Build

```bash
yarn && yarn build
```

# Dealing With CORS

## **Note: Added a custom nextjs server that now handles the proxy. This is not needed anymore, but keeping it here just for the awareness of the plugin**
- nextjs doesn't use the webpack dev server, so we can't proxy without a custom server. Not a big deal to add that, but we don't have it right now because there are more important issues to take care of.
- To get around CORS issues, you can use the cors-everywhere extension - https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/.
  - Chrome probably has an equivalent

# Todo
- Add star count to tutorials
- Allow users to "star" a tutorial to indicate it was helpful
- Add ability for users to attach links to goals
- Add ability for users to attach files (Up to 2MB in size) to goals, upload them to the DO space and store the link
- 

# Handling getInitialProps

- Most pages need to have the current user profile loaded. This can be handled in the getInitialProps function for the page.
    - Both server side render and client side render scenarios must be handled. req is only defined when the function is called on the server. The cookie must be passed to initApollo as the second parameter (an empty object can be passed for initial state), and the cookie can be retrieved from req.headers.cookie

## Future
- Add ability for users to comment on tutorials
- Allow users to upload an avatar image
  - Need to allow users to report any links/image uploads